﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Web.Http;
using ComplainAPI.Controllers;
using ComplainAPI.Models;
using DataModel;
using SynchonousAPITest.Tests.Models;

namespace SynchonousAPI.Tests
{
    [TestClass]
    public class OTPTestCase
    {
        [TestMethod]
        public void sendOTP()
        {
            var controller = new OTPServiceController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            UserModel model = new UserModel();
            model.UserName = "919682496133";
            var response = controller.sendotp(model);

            // Assert the result  
            ResponseModel _model;
            Assert.IsTrue(response.TryGetContentValue<ResponseModel>(out _model));
            Assert.AreEqual(response.StatusCode, System.Net.HttpStatusCode.OK);
            Assert.AreEqual("OTP has been sent, use remark value for OTP verification", _model.Desc);
        }

        [TestMethod]
        public void ValidateOtpOTP()
        {
            var controller = new OTPServiceController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            OTPModel otpModel = new OTPModel();
            otpModel.MessageSID = "SM48379ed382c14ebd96a2f5d8598c46d8";
            otpModel.OTP = "5425";
            var response = controller.ValidateOtp(otpModel);

            // Assert the result  
            ResponseModel _model;
            Assert.IsTrue(response.TryGetContentValue<ResponseModel>(out _model));
            Assert.AreEqual(response.StatusCode, System.Net.HttpStatusCode.OK);
        }
    }
}
