﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Web.Http;
using ComplainAPI.Controllers;
using ComplainAPI.Models;
using DataModel;


namespace SynchonousAPI.Tests
{
    [TestClass]
    public class AuthTestController
    {
        [TestMethod]
        public void Login()
        {
            var controller = new AuthController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            controller.Request.Headers.Add("password", "samsam@123");
            var response = controller.Login("919682496133");
            
            // Assert the result  
            Login login;
            Assert.IsTrue(response.TryGetContentValue<Login>(out login));
            Assert.AreEqual("919682496133", login.Mobile);
        }

        [TestMethod]
        public void setPassword()
        {
            var controller = new AuthController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            controller.Request.Headers.Add("password", "samsam@123");
            var response = controller.Login("919682496133");

            // Assert the result  

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void Forgetpassword()
        {
            var controller = new AuthController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            var response = controller.Login("919682496133");

            
            Assert.IsNotNull(response.StatusCode.Equals(200));
        }

        [TestMethod]
        public void ChangePassword()
        {
            var controller = new AuthController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            controller.Request.Headers.Add("oldpassword", "samsam@123");
            controller.Request.Headers.Add("newPassword", "samsam@1235");
            var response = controller.Login("919682496133");

            // Assert the result  

            Assert.IsNotNull(response);
        }

        [TestMethod]
        public void MobileVerify()
        {
            var controller = new AuthController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            var response = controller.Login("919682496133");


            Assert.IsNotNull(response.StatusCode.Equals(200));
        }
    }
}
