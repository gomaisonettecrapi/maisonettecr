﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SynchonousAPITest.Tests.Models
{
   public class ResponseModel
    {
        public int statusCode { get; set; }
        public string statusText { get; set; }
        public string Desc { get; set; }
        public string Remark { get; set; }
    }
    
}
