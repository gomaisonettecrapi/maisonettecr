﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Net.Http;
using System.Web.Http;
using ComplainAPI.Controllers;
using ComplainAPI.Models;
using DataModel;
using SynchonousAPITest.Tests.Models;

namespace SynchonousAPI.Tests
{
    [TestClass]
    public class TicketsTestCase
    {
        [TestMethod]
        public void TicketList()
        {
            var controller = new TicketController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            TicketListModel model = new TicketListModel();
            model.TenantId = 1;
            var response = controller.TicketList(model);

            // Assert the result  
            GetTicketList_Result modelResult;
            Assert.IsTrue(response.TryGetContentValue<GetTicketList_Result>(out modelResult));
            Assert.AreEqual(response.StatusCode, System.Net.HttpStatusCode.OK);
            Assert.AreEqual(1, modelResult.TenantId);
        }

        [TestMethod]
        public void TicketDetails()
        {
            var controller = new TicketController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            TicketDetailsModel model = new TicketDetailsModel();
            model.TicketId = 2;
            var response = controller.TicketDetails(model);

            // Assert the result  
            GetTicketDetails_Result modelResult;
            Assert.IsTrue(response.TryGetContentValue<GetTicketDetails_Result>(out modelResult));
            Assert.AreEqual(response.StatusCode, System.Net.HttpStatusCode.OK);
            Assert.AreEqual(model.TicketId, modelResult.TicketId);
        }
    }
}
