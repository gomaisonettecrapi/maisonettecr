﻿

namespace SynchonousAPI.Tests
{
    using System;
    using Microsoft.VisualStudio.TestTools.UnitTesting;
    using System.Net.Http;
    using System.Web.Http;
    using ComplainAPI.Controllers;
    using ComplainAPI.Models;
    using DataModel;
    using SynchonousAPITest.Tests.Models;

    [TestClass]
    public class UserInfoTestCase
    {
        [TestMethod]
        public void UserInfo()
        {
            var controller = new UsersController();
            controller.Request = new HttpRequestMessage();
            controller.Configuration = new HttpConfiguration();
            TenantDetails tenant = new TenantDetails();
            tenant.TenantId = 1;
            var response = controller.userinfo(tenant);

            // Assert the result 
            Assert.AreEqual(response.StatusCode, System.Net.HttpStatusCode.OK);
        }
    }
}
