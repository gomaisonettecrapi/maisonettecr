﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace TechnicianAPI.Models
{
    public class LoginModel : UserModel
    {

        [Required(AllowEmptyStrings = false, ErrorMessage = "Password is required")]
        [StringLength(maximumLength: 18, MinimumLength = 6, ErrorMessage = "Invalid Password provided in parameter")]
        public string password { get; set; }
    }

    public class UserModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "Username is required")]
        [StringLength(maximumLength: 14, MinimumLength = 10, ErrorMessage = "Invalid Username provided in parameter")]
        [RegularExpression(@"^(\d{8,14})$", ErrorMessage = "Invalid Username/Mobile No.")]
        public string UserName { get; set; }

        [StringLength(maximumLength: 50, MinimumLength = 34, ErrorMessage = "Invalid MessageId provide in parameter(Min char 34)")]
        [RegularExpression(@"^[a-zA-Z][a-zA-Z0-9]*$", ErrorMessage = "Invalid MessageId provide in parameter(Only Alphanumeric)")]
        public string MessageId { get; set; }
    }

    public class SignoutModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "PersonId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "PersonId should be greater than zero(0)")]
        public int PersonId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "ClientId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "ClientId should be greater than zero(0)")]
        public int ClientId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Username is required")]
        [StringLength(maximumLength: 14, MinimumLength = 10, ErrorMessage = "Invalid Username provided in parameter")]
        [RegularExpression(@"^(\d{8,14})$", ErrorMessage = "Invalid Username/Mobile No.")]
        public string UserName { get; set; }
    }

    public class NotifyDetails
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "DeviceId is required")]
        public string DeviceId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "DeviceRegToken is required")]
        public string DeviceRegToken { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "DeviceType is required")]
        public string DeviceType { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "PersonId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "PersonId should be greater than zero(0)")]
        public int PersonId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Username is required")]
        [StringLength(maximumLength: 14, MinimumLength = 10, ErrorMessage = "Invalid Username provided in parameter")]
        [RegularExpression(@"^(\d{8,14})$", ErrorMessage = "Invalid Username/Mobile No.")]
        public string UserName { get; set; }
    }
}