﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace TechnicianAPI.Models
{
    public class PersonDetails
    {
        /// <summary>
        /// Get: retrive TenantId value
        /// Set: set TenantId value
        /// </summary>
        [Required(AllowEmptyStrings =false,ErrorMessage = "PersonId is Required")]
        [Range(1,int.MaxValue,ErrorMessage = "PersonId should be greater than 0(zero)")]
        public int PersonId { get; set; }
    }
}