﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace TechnicianAPI.Models
{

    public class WorkOrderListModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "PersonId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "PersonId should be greater than 0 (zero)")]
        public int PersonId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "ClientId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "ClientId should be greater than 0 (zero)")]
        public int ClientId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "RoleType is required")]
        [Range(1, 2, ErrorMessage = "RoleType should be 1 or 2")]
        public int RoleType { get; set; }

        
    }
    public class ClientModel
    {
        /// <summary>
        /// Get: Retrive ClientId
        /// Set: Set ClientId
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "ClientId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "ClientId should be greater than 0 (zero)")]
        public int ClientId { get; set; }
    }
    public class StatusModel 
    {
        /// <summary>
        /// Get: Retrive StatusId
        /// Set: Set StatusId
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "StatusId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "StatusId should be greater than 0 (zero)")]
        public int StatusId { get; set; }


        [Required(AllowEmptyStrings = false, ErrorMessage = "WorkOrderId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "WorkOrderId should be greater than 0 (zero)")]
        public int WorkOrderId { get; set; }
    }
    
    public class WorkDetailsModel : ClientModel
    {
        /// <summary>
        /// Get: Retrive Size
        /// Set: Set Size
        /// </summary>
      

        [Required(AllowEmptyStrings = false, ErrorMessage = "WorkOrderId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "WorkOrderId should be greater than 0 (zero)")]
        public int WorkOrderId { get; set; }
    }
    public enum RoleType
    {
        Technician = 1,
        Supervisor=2
    }

    public enum TicketStatus
    {
        AllTickets = 0,
        Sent = 1,
        Processing,
        InProgress,
        Closed,
        Cancelled
    }
    public class WorkModel
    {
    }
}