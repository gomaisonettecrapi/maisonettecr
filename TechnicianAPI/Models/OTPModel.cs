﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace TechnicianAPI.Models
{
    public class OTPModel
    {
        [Required(AllowEmptyStrings =false,ErrorMessage = "MessageSID is required")]
        [StringLength(maximumLength: 50, MinimumLength = 34, ErrorMessage = "Invalid MessageSID provide in parameter(should be min 34 Char)")]
        [RegularExpression(@"^[a-zA-Z][a-zA-Z0-9]*$", ErrorMessage = "Invalid MessageSID provide in parameter(Only Alphanumeric)")]
        public string MessageSID { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "OTP is required")]
        [StringLength(maximumLength:4,MinimumLength =4,ErrorMessage ="Invalid OTP provide in parameter(should be only 4 digit)")]
        [RegularExpression(@"^(\d{4})$",ErrorMessage = "Invalid OTP provide in parameter(Only Digit)")]
        public string OTP { get; set; }
    }
}