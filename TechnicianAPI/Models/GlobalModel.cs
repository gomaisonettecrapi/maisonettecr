﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Http;
using System.Web.Http;
using System.Net;

namespace TechnicianAPI.Models
{
    public class SingleParam<T>
    {
        public T Param { get; set; }
    }
}