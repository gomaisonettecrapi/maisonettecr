﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net.Http;
using System.Net;
using TechnicianAPI.Models;

namespace TechnicianAPI.Filter
{
    public class ValidateAPIKey: ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            try
            {
                IEnumerable<string> apiKey = actionContext.Request.Headers.GetValues("api_key");
                if (apiKey != null)
                {
                    if (!Utility.CompairAPIKey(apiKey.FirstOrDefault()))
                    {
                        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, new { Desc = "API Key : invalid" }, actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
                        return;
                    }
                }
                else
                {
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, new { Desc = "API Key : Not Found" }, actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
                    return;
                }
            }
            catch (Exception ex)
            {
                actionContext.Response= actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, new { Desc = "API Key : "+ex.Message },actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
            }
        }
    }
}