﻿using DataModel;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using TechnicianAPI.Filter;
using TechnicianAPI.Models;

namespace TechnicianAPI.Controllers
{
    [RoutePrefix("api/auth")]
    [ValidateAPIKey]
    public class AuthController : OTPServiceController
    {
        [HttpPost]
        [Route("Forgetpassword")]
        [Authorize]
        public HttpResponseMessage Forgetpassword([FromBody] UserModel input)
        {
            if (input != null && ModelState.IsValid)
            {
                try
                {
                    using (MaisonetteEntities entity = new MaisonetteEntities())
                    {
                        var loginCollection = entity.PersonLogins.Where(x => (x.ISDCode + x.Mobile).Equals(input.UserName) && x.IsActive == true).FirstOrDefault(); // Mobile is treated as Username

                        if (loginCollection != null)
                        {
                            try
                            {
                                string messageId = CreateOTP(input.UserName);
                                loginCollection.IsVerified = false;
                                loginCollection.ModificationDate = DateTime.Now;
                                entity.Entry(loginCollection).State = EntityState.Modified;
                                entity.SaveChanges();
                                return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Successful, "OTP has been sent, use remark value for OTP verification", new { MessageId = messageId }));
                            }
                            catch (Exception ex)
                            {
                                return CatchException(ex);
                            }
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Username not exist"));
                        }
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Inpalid Paramater", GetErrorList()));
            }
        }

        /// <summary>
        /// sent reset OTP to verify username and change password 
        /// </summary>
        /// <param name="MobileNo">919682496133</param>
        /// <returns>status code and Forget password status</returns>
        [HttpPost]
        [Route("Signout")]
        [Authorize]
        public HttpResponseMessage Signout([FromBody] SignoutModel input)
        {
            if (input != null && ModelState.IsValid)
            {
                try
                {
                    using (MaisonetteEntities entity = new MaisonetteEntities())
                    {
                        var loginCollection = entity.PersonLogins.Where(x => (x.ISDCode + x.Mobile).Equals(input.UserName) && x.ClientId.Equals(input.ClientId) && x.PersonId.Equals(input.PersonId) && x.IsActive == true).FirstOrDefault();
                        if (loginCollection != null)
                        {
                            try
                            {
                                loginCollection.DeviceId = string.Empty;
                                loginCollection.DeviceRegToken = string.Empty;
                                loginCollection.DeviceType = string.Empty;
                                loginCollection.ModificationDate = DateTime.Now;
                                entity.Entry(loginCollection).State = EntityState.Modified;
                                int effectedRow = entity.SaveChanges();
                                if (effectedRow > 0)
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Signout successfully"));
                                }
                                else
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Signout failed"));
                                }

                            }
                            catch (Exception ex)
                            {
                                return CatchException(ex);
                            }
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Technician/Supervisor Id " + input.PersonId + " is not mapped with Client id " + input.ClientId));
                        }
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Inpalid Paramater", GetErrorList()));
            }
        }

        /// <summary>
        /// To set new password 
        /// </summary>
        /// <param name="username">Mobile no. along country code as username</param>
        /// <returns>Status code and set password status</returns>
        [HttpPost]
        [Route("setPassword")]
        public HttpResponseMessage SetPassword([FromBody] UserModel input)
        {
            try
            {
                if (Request.Headers.Contains("password"))
                {
                    IEnumerable<string> value = Request.Headers.GetValues("password");
                    var password = value.FirstOrDefault();
                    if (input != null && ModelState.IsValid && GenaricValidation.StringMinLen(new string[] { password }, 6))
                    {
                        using (MaisonetteEntities entity = new MaisonetteEntities())
                        {
                            string plainText = Utility.Decode64String(password);
                            if (!GenaricValidation.Password(plainText))
                            {
                                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Password", "Password should contain at least one upper case,one lower case,one digit,one special character and minimum 8 in length"));
                            }
                            var loginCollection = entity.PersonLogins.Where(x => (x.ISDCode + x.Mobile).Equals(input.UserName) && x.IsActive == true).FirstOrDefault<PersonLogin>();

                            if (loginCollection != null)
                            {
                                loginCollection.PasswordHash = Utility.GetHashString(plainText);
                                loginCollection.ModificationDate = DateTime.Now;
                                entity.SaveChanges();
                                return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Password has been set"));
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.Unauthorized, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Mobile Number is not registered"));
                            }
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Some Header Parameter are not supplied"));
                }
            }
            catch (Exception ex)
            {
                return CatchException(ex);
            }
        }

        /// <summary>
        /// change existing password
        /// </summary>
        /// <param name="username">Mobile no. along country code as username</param>
        /// <returns></returns>
        [HttpPost]
        [Authorize]
        [Route("changePassword")]
        public HttpResponseMessage ChangePassword([FromBody] UserModel input)
        {
            try
            {
                if (Request.Headers.Contains("oldpassword") && Request.Headers.Contains("newPassword"))
                {
                    IEnumerable<string> oldValue = Request.Headers.GetValues("oldpassword");
                    IEnumerable<string> newValue = Request.Headers.GetValues("newPassword");

                    var oldPassword = oldValue.FirstOrDefault();
                    var newPassword = newValue.FirstOrDefault();

                    if (input != null && ModelState.IsValid && GenaricValidation.StringMinLen(new string[] { newPassword, oldPassword }, 6))
                    {

                        oldPassword = Utility.Decode64String(oldPassword);
                        newPassword = Utility.Decode64String(newPassword);
                        if (oldPassword.ToLower().Equals(newPassword.ToLower()))
                        {
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Password", "New Password should not be same as old password"));
                        }

                        if (!GenaricValidation.Password(newPassword))
                        {
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Password", "New Password should contain at least one upper case,one lower case,one digit,one special character and minimum 8 in length"));
                        }
                        using (MaisonetteEntities entity = new MaisonetteEntities())
                        {
                            string hashPasswordOld = Utility.GetHashString(oldPassword);
                            string hashPasswordNew = Utility.GetHashString(newPassword);
                            var loginCollection = entity.PersonLogins.Where(x => (x.ISDCode + x.Mobile).Equals(input.UserName) && x.PasswordHash.Equals(hashPasswordOld) && x.IsActive == true).FirstOrDefault();

                            if (loginCollection != null)
                            {
                                if (loginCollection.IsVerified)
                                {

                                    loginCollection.PasswordHash = hashPasswordNew;
                                    loginCollection.ModificationDate = DateTime.Now;
                                    entity.Entry(loginCollection).State = EntityState.Modified;
                                    int saveRows = entity.SaveChanges();
                                    if (saveRows > 0)
                                    {
                                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Password has Changed"));
                                    }
                                    else
                                    {
                                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Password has not Changed"));
                                    }
                                }
                                else
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Mobile No. is not verified"));
                                }
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.Unauthorized, GenericStatus.GetGenericStatus(GenericStatusCode.InvalidCredential, "Invalid Credential"));
                            }
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
                    }

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Some Header parameter are missing"));
                }
            }
            catch (Exception ex)
            {
                return CatchException(ex);
            }
        }

        [HttpPost]
        [Route("MobileVerify")]
        public HttpResponseMessage MobileVerify([FromBody] UserModel input)
        {
            if (input != null && ModelState.IsValid)
            {
                try
                {
                    using (MaisonetteEntities entity = new MaisonetteEntities())
                    {

                        var loginCollection = entity.PersonLogins.Where(x => (x.ISDCode + x.Mobile).Equals(input.UserName)).FirstOrDefault();

                        if (loginCollection != null)
                        {
                            loginCollection.PasswordHash = null;
                            object response = new { ClientId = loginCollection.ClientId, PersonId = loginCollection.PersonId, MobileNo = loginCollection.ISDCode + loginCollection.Mobile, IsOTPVerified = loginCollection.IsVerified, IsActive = loginCollection.IsActive, LastLogin = loginCollection.LastLogin, LoginId = loginCollection.LoginId };
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Successful, input.UserName + " is found in database", response));
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, input.UserName + " is not registered with Maisonette system"));
                        }
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);

                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter supplied", GetErrorList()));
            }
        }

        private IEnumerable<ModelError> GetErrorList()
        {
            return ModelState.Values.SelectMany(v => v.Errors);
        }

        private HttpResponseMessage CatchException(Exception ex)
        {
            SqlException sqlException = ex.InnerException.InnerException as SqlException;
            if (sqlException != null)
            {
                switch (sqlException.Number)
                {
                    case 2601:
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.UniqueKeyViolation, "Cant insert Duplicate Key value", sqlException.Message));
                    case 2627:
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.PrimaryKeyViolation, "Cant insert Duplicate Key value", sqlException.Message));
                    case 547:
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ForeignKeyViolation, "Cant insert which is not exist in Master Data", sqlException.Message));
                    case 515:
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.NullValueViolation, "Cant insert Null value", sqlException.Message));
                    default:
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.InvalidValue, "Cant insert Duplicate Key value", sqlException.Message));
                }
            }
            else
                return Request.CreateResponse(HttpStatusCode.InternalServerError, GenericStatus.GetGenericStatus(GenericStatusCode.InvalidValue, "Some Invalid Value Supplied" + ex.Message));
        }
    }
}
