﻿    using TechnicianAPI.Filter;
    using TechnicianAPI.Models;
    using DataModel;
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data.Entity;
    using System.Data.SqlClient;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Http;
    using System.Web.Http.ModelBinding;
    using Newtonsoft.Json;
    using System.Threading;

namespace TechnicianAPI.Controllers
{
    [ValidateAPIKey]
    [RoutePrefix("api/WorkOrder")]
    [Authorize]
    public class WorkOrderController : ApiController
    {

        [HttpPost]
        [Route("WorkOrderList")]
        public HttpResponseMessage WorkOrderList([FromBody] WorkOrderListModel inputParameter)
        {
            try
            {
                if (inputParameter != null && ModelState.IsValid)
                {
                    MaisonetteEntities entity = new MaisonetteEntities();
                   
                    var ClientTableRow = entity.MaintainacePersons.Where(column => column.ClientId.Equals(inputParameter.ClientId)).FirstOrDefault();
                    var WorkStatusRow = entity.WorkOrders.Where(column => column.PersonId.Equals(inputParameter.PersonId)).FirstOrDefault();
                    var WorkStatusName = Enum.GetName(typeof(Utility.WorkOrderStatus), WorkStatusRow.WorkOrderStatus);
                  
                    if (ClientTableRow == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotMatched, "Client Id: " + inputParameter.ClientId + " is not mapped with Technician/Supervisor"));
                    }                   
                       
                  
                    var result = (
                                    from tics in entity.Tickets
                                    join req in entity.RequestTypeMasters on tics.RequestTypeId equals req.RequestTypeId
                                    join status in entity.TicketStatusMasters on tics.StatusId equals status.Id
                                    join Work in entity.WorkOrders on tics.Id equals Work.TicketId
                                    join Maintainace in entity.MaintainacePersons on tics.ClientId equals Maintainace.ClientId
                                    join Component in entity.Components on Maintainace.ComponentId equals Component.ComponentId

                                    where req.IsActive && status.IsActive && Maintainace.PersonId == inputParameter.PersonId && Maintainace.RoleType==inputParameter.RoleType
                                    && tics.IsActive == true && tics.ClientId.Equals(req.ClientId) && Maintainace.IsActive==true && Work.IsActive==true && tics.StatusId < (int)TicketStatus.Closed
                                    && tics.ClientId.Equals(inputParameter.ClientId) && req.ClientId.Equals(inputParameter.ClientId ) 
                                    orderby tics.Id descending
                                    select new {
                                        tics.TenantId, tics.TicketNo,
                                        TicketId = tics.Id,
                                        tics.RequestTitle,
                                        tics.RequestDecription,
                                        tics.Priority,
                                        tics.ClientId,
                                        status.StatusType,
                                        tics.IsSync,
                                        req.RequestTypeId,
                                        req.RequestType,
                                        tics.CreationDate,
                                        tics.LocationType,
                                        tics.LocationId,
                                        Maintainace.ComponentId,
                                        Role = Maintainace.RoleType == (int)Utility.RoleType.Technician ? Utility.RoleType.Technician.ToString() : Utility.RoleType.Supervisor.ToString(),
                                        WorkStatus= WorkStatusName,
                                        Work.WorkOrderId,                                        
                                        Maintainace.PersonId,
                                        Maintainace.ContractStartDate,
                                        Maintainace.ContractEndDate,
                                        Maintainace.SupervisorId
                                    }

                                 );

                    if (result != null && result.ToList().Count > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataFound, "Record found", result));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "No workorder found for person Id : " + inputParameter.PersonId, null));
                    }

                }
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid person Id", GetErrorList()));
            }
            catch (Exception ex)
            {
                return CatchException(ex);
            }
        }


        [HttpPost]
        [Route("WorkDetails")]
        public HttpResponseMessage WorkDetails([FromBody] WorkDetailsModel input)
        {
            if (input != null && ModelState.IsValid)
            {
                try
                {
                    MaisonetteEntities entity = new MaisonetteEntities();
                    var WorkDetails = (
                                from tics in entity.Tickets
                                from req in entity.RequestTypeMasters.Where(mapping => mapping.RequestTypeId == tics.RequestTypeId)
                                from status in entity.TicketStatusMasters.Where(mapping => mapping.Id == tics.StatusId)
                                from unt in entity.Units.Where(mapping => mapping.UnitId == tics.LocationId).DefaultIfEmpty()
                                from compo in entity.Components.Where(mapping => mapping.ComponentId == tics.LocationId).DefaultIfEmpty()

                                from Work in entity.WorkOrders.Where(mapping => mapping.TicketId == tics.Id && mapping.ClientId==input.ClientId).DefaultIfEmpty()
                                from client in entity.Clients.Where(mapping => mapping.ClientID == Work.ClientId).DefaultIfEmpty()
                                from maintaince in entity.MaintainacePersons.Where(mapping => mapping.PersonId == Work.PersonId && mapping.ComponentId==compo.ComponentId).DefaultIfEmpty()

                                where 
                                req.IsActive && status.IsActive && tics.IsActive && status.IsActive && compo.IsActive && Work.IsActive && client.IsActive && maintaince.IsActive
                                select new
                                {
                                    tics.TenantId,
                                    tics.TicketNo,
                                    TicketId = tics.Id,
                                    tics.RequestTitle,
                                    tics.RequestDecription,
                                    tics.Priority,
                                    tics.ClientId,
                                    status.StatusType,
                                    tics.IsSync,
                                    req.RequestTypeId,
                                    req.RequestType,
                                    tics.CreationDate,
                                    tics.LocationType,
                                    tics.LocationId,
                                    LocationIdName = (tics.LocationType == 1 ? compo.ComponentName : unt.UnitNumber),                                   
                                    tics.StatusId,
                                    Work.RequestVisitDate,
                                    Work.WorkOrderId,
                                    Role = maintaince.RoleType == (int)Utility.RoleType.Technician ? Utility.RoleType.Technician.ToString() : Utility.RoleType.Supervisor.ToString(),
                                    ApprovalStatus= Work.IsSupervisorApproved  ? Utility.Approved.ApprovedBySupervisor.ToString() : Utility.Approved.NotApprovedBySupervisor.ToString(),
                                    Work.JobStartDate,
                                    Work.JobEndDate

                                }
                             );
                    if (WorkDetails != null && WorkDetails.ToList().Count() > 0)
                    {
                       
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataFound, "Record found", new { WorkDetails = WorkDetails}));
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "No work detail found for WorkOrderId Id : " + input.WorkOrderId + " and ClientId : " + input.ClientId, null));
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Ticket Id", GetErrorList()));

        }

        [HttpPost]
        [Route("UpdateStatus")]
        public HttpResponseMessage UpdateStatus([FromBody] StatusModel input)
        {
            if (input != null && ModelState.IsValid)
            {
                try
                {
                    using (MaisonetteEntities entity = new MaisonetteEntities())
                    {
                       
                        var WorkStatus = entity.WorkOrders.Where(x =>  x.IsActive && x.WorkOrderId == input.WorkOrderId).FirstOrDefault();
                        if (WorkStatus != null)
                        {

                            bool isValidStatusId = Enum.IsDefined(typeof(Utility.WorkOrderStatus), input.StatusId);
                            if (isValidStatusId)
                            {
                                WorkStatus.WorkOrderStatus = input.StatusId;
                                WorkStatus.ModificationDate = DateTime.Now;
                                WorkStatus.IsSync = false;
                                entity.Entry(WorkStatus).State = EntityState.Modified;
                                int SaveRows = entity.SaveChanges();
                                if (SaveRows > 0)
                                {

                                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Status updated"));
                                }
                                else
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Status not updated"));
                                }
                            }
                            else
                                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "StatusId doesn't exist in master database"));
                        }
                        else
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "No workorder Id found for  : " + input.WorkOrderId, null));
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid TicketId/StatusId Supplied", GetErrorList()));
        }

        private IEnumerable<ModelError> GetErrorList()
        {
            return ModelState.Values.SelectMany(v => v.Errors);
        }

        private HttpResponseMessage CatchException(Exception ex)
        {
            try
            {
                SqlException sqlException = ex.InnerException.InnerException as SqlException;
                if (sqlException != null)
                {
                    switch (sqlException.Number)
                    {
                        case 2601:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.UniqueKeyViolation, "Cant insert Duplicate Key value", sqlException.Message));
                        case 2627:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.PrimaryKeyViolation, "Cant insert Duplicate Key value", sqlException.Message));
                        case 547:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ForeignKeyViolation, "Cant insert which is not exist in Master Data", sqlException.Message));
                        case 515:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.NullValueViolation, "Cant insert Null value", sqlException.Message));
                        default:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.InvalidValue, "Cant insert Duplicate Key value", sqlException.Message));
                    }
                }
                else
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, GenericStatus.GetGenericStatus(GenericStatusCode.InvalidValue, "Some Invalid Value Supplied" + ex.Message));
            }
            catch (Exception exx)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, GenericStatus.GetGenericStatus(GenericStatusCode.InvalidValue, "Some Invalid Value Supplied" + exx.Message));
            }
        }

    }
}
