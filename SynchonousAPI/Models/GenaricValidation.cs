﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;

namespace SynchronousAPI.Models
{
    public static class GenaricValidation
    {
        #region GLOBAL VARIABLE
        private const string Rex10D = @"\+?[0-9]{10}";
        private const string Rex14D = @"\+?[0-9]{11,14}$";
        #endregion

        #region Regex Validation
        public static bool Mobile10D(string mobileNo)
        {
            mobileNo = mobileNo.Replace(" ", "").Replace("-", "");
            return Regex.IsMatch(mobileNo, Rex10D);
        }

        public static bool Mobile14D(string mobileNo)
        {
            mobileNo = mobileNo.Replace(" ", "").Replace("-", "");
            return Regex.IsMatch(mobileNo, Rex14D);
        }
        #endregion

        #region String Length Validation
        public static bool StringMinLen(string val, int minLen)
        {
            return val.Trim().Length >= minLen ? true : false;
        }

        public static bool StringMinLen(string[] val, int minLen)
        {
            return val.Select(x => x.Trim().Length >= minLen).Count() == val.Length ? true : false;
        }

        public static bool StringMaxLen(string val, int maxLen)
        {
            return val.Trim().Length <= maxLen ? true : false;
        }

        public static bool StringMaxLen(string[] val, int maxLen)
        {
            return val.Select(x => x.Trim().Length <= maxLen).Count() == val.Length ? true : false;
        }

        public static bool StringFixLen(string val, int fixLen)
        {
            return val.Trim().Length == fixLen ? true : false;
        }

        public static bool StringFixLen(string[] val, int fixLen)
        {
            return val.Select(x => x.Trim().Length == fixLen).Count() == val.Length ? true : false;
        }
        #endregion
    }
}