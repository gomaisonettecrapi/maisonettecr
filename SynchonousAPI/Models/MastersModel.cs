﻿namespace SynchronousAPI.Models
{
    using System.ComponentModel.DataAnnotations;
    using DataModel;
    using System;
    using System.Collections.Generic;

    /// <summary>
    ///ClientId Model
    /// </summary>
    public class TicketIdModel
    {
        /// <summary>
        /// get: retrive IsActive
        /// set: set IsActive
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "TicketId is required")]
        [StringLength(maximumLength:20, MinimumLength =4, ErrorMessage = "TicketNo Should be min 4 chars")]
        public string TicketNo { get; set; }
    }

    /// <summary>
    ///ClientId Model
    /// </summary>
    public class ClientIdModel
    {
        /// <summary>
        /// get: retrive IsActive
        /// set: set IsActive
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "ClientId is required")]
        [Range(1,int.MaxValue,ErrorMessage ="ClientId Should be greater than 0 (zero)")]
        public int ClientId { get; set; }
    }

    /// <summary>
    /// Generic table activation model
    /// </summary>
    public class ActivationModel
    {
        /// <summary>
        /// get: retrive IsActive
        /// set: set IsActive
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Invalid IsActive Status")]
        public bool IsActive { get; set; }
    }

    /// <summary>
    /// Insert for bulk data in database From EF
    /// </summary>
    public static class InsertBulk
    {
        /// <summary>
        /// Add Entity to data database context and after specified entity added it will commit in database
        /// </summary>
        /// <typeparam name="T">Database Entity type</typeparam>
        /// <param name="context">Database context</param>
        /// <param name="entity">Database Entity</param>
        /// <param name="count">No. of current count</param>
        /// <param name="commitCount">No. of count after it will commit</param>
        /// <param name="recreateContext"></param>
        /// <returns></returns>
        public static MaisonetteEntities AddToContext<T>(MaisonetteEntities dbContext, T entity, int thresholdCount, int CurrentCount, bool recreateContext) where T : class
        {
            dbContext.Set<T>().Add(entity);

            if (thresholdCount % CurrentCount == 0)
            {
                dbContext.SaveChanges();
                if (recreateContext)
                {
                    dbContext.Dispose();
                    dbContext = new MaisonetteEntities();
                    dbContext.Configuration.AutoDetectChangesEnabled = false;
                }
            }

            return dbContext;
        }
    }

    /// <summary>
    /// Component table scheme
    /// </summary>
    public class ComponentModel: ClientIdModel
    {
    //    [Required(ErrorMessage = "ClientID is required")]
    //    [Range(1, int.MaxValue, ErrorMessage = "ClientID should be greater than 0 (zero)")]
    //    public int ClientID { get; set; }

        [Key]
        [Required(ErrorMessage = "ComponentId  is required")]
        [Range(1,int.MaxValue,ErrorMessage= "ComponentId should be greater than 0 (zero)")]
        public int ComponentId { get; set; }

        [Required(AllowEmptyStrings =false,ErrorMessage = "ComponentNumber is required")]
        [StringLength(maximumLength: 80, ErrorMessage = "Invalid ComponentNumber (Max: 80 Char,Min:3 char)", MinimumLength = 3)]
        public string ComponentNumber { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "ComponentName is required")]
        [StringLength(maximumLength: 100, ErrorMessage = "Invalid ComponentName (Max: 100 Char,Min:1 char)", MinimumLength = 1)]
        public string ComponentName { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Address is required")]
        [StringLength(maximumLength: 100, ErrorMessage = "Invalid Address (Max: 100 Char,Min:3 char)", MinimumLength = 3)]
        public string Address { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "PlotNo is required")]
        [StringLength(maximumLength: 10, ErrorMessage = "Invalid PlotNo (Max: 10 Char,Min:1 char)", MinimumLength = 1)]
        public string PlotNo { get; set; }

        public string Sector { get; set; }

        public string Region { get; set; }

        public string PoBox { get; set; }

        [Required(AllowEmptyStrings =false, ErrorMessage = "Nationality is required")]
        [StringLength(maximumLength: 100, ErrorMessage = "Invalid Nationality (Max: 100 Char,Min:3 char)", MinimumLength = 3)]
        public string Nationality { get; set; }

        [Required(AllowEmptyStrings =false, ErrorMessage = "City is required")]
        [StringLength(maximumLength: 100, ErrorMessage = "Invalid City (Max: 100 Char,Min:3 char)", MinimumLength = 3)]
        public string City { get; set; }
    }

    /// <summary>
    /// Login table scheme
    /// </summary>
    public class LoginModel: ClientIdModel
    {
        /// <summary>
        /// get: retrive ClientID
        /// set: set ClientID
        /// </summary>
        //[Required(ErrorMessage = "ClientID is required")]
        //[Range(1, int.MaxValue, ErrorMessage = "ClientID should be greater than 0 (zero)")]
        //public int ClientID { get; set; }

        /// <summary>
        /// get: retrive TenantId
        /// set: set TenantId
        /// </summary>
        [Required(ErrorMessage = "TenantId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "TenantId should be greater than 0 (zero)")]
        [Key]
        public int TenantId { get; set; }

        /// <summary>
        /// get: retrive Mobile
        /// set: set Mobile
        /// </summary>
        [Required(AllowEmptyStrings =false,ErrorMessage = "Mobile number is required")]
        [StringLength(maximumLength: 15, ErrorMessage = "Invalid Mobile number (Min:5 Digit,Max:15 Digit)", MinimumLength = 5)]
        public string Mobile { get; set; }

        /// <summary>
        /// get: retrive international ISD code
        /// set: set international ISD code
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "ISD Code is required")]
        [StringLength(maximumLength: 4, ErrorMessage = "Invalid ISD Code (Min:1 char, Max:4 char)", MinimumLength = 1)]
        public string ISDCode { get; set; }

    }

    /// <summary>
    /// Client table scheme
    /// </summary>
    public class ClientModel: ClientIdModel
    {
        /// <summary>
        /// get: retrive iClientID
        /// set: set ClientID
        /// </summary>
        //[Required(ErrorMessage = "ClientID is required")]
        //[Range(1, int.MaxValue, ErrorMessage = "ClientID should be greater than 0 (zero)")]
        //[Key]
        //public int ClientID { get; set; }

        /// <summary>
        /// get: retrive Client Number
        /// set: set Client Number
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "ClientNumber is required")]
        [StringLength(maximumLength:20, ErrorMessage= "Invalid ClientNumber (Min:1 char, Max:20 char)", MinimumLength =1)]
        public string ClientNumber { get; set; }

        /// <summary>
        /// get: retrive Client Name
        /// set: set Client Name
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "Client Name is required")]
        [StringLength(maximumLength: 200, ErrorMessage = "Invalid Name (Min:3 char, Max:200 char)", MinimumLength = 3)]
        public string Name { get; set; }

        /// <summary>
        /// get: retrive PersronName
        /// set: set PersronName
        /// </summary>
        //[Required(AllowEmptyStrings =false, ErrorMessage = "Person Name is required")]
        [StringLength(maximumLength: 200, ErrorMessage = "Person Name (Min:3 char, Max:200 char)", MinimumLength = 3)]
        public string PersonName { get; set; }

        /// <summary>
        /// get: retrive Address
        /// set: set Address
        /// </summary>
        /// remove required changes by suraj date 24 aug 2017

        //[Required(AllowEmptyStrings = false, ErrorMessage = "Address is required")]
        [StringLength(maximumLength: 200, ErrorMessage = "Invalid Address (Min:3 char, Max:200 char)", MinimumLength = 3)]
        public string Address { get; set; }

        /// <summary>
        /// get: retrive Nationality
        /// set: set Nationality
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Nationality is required")]
        [StringLength(maximumLength: 100, ErrorMessage = "Invalid Nationality (Min:3 char, Max:100 char)", MinimumLength = 3)]
        public string Nationality { get; set; }

        /// <summary>
        /// get: retrive City
        /// set: set City
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "City is required")]
        [StringLength(maximumLength: 100, ErrorMessage = "Invalid City (Min:3 char, Max:100 char)", MinimumLength = 3)]
        public string City { get; set; }

        /// <summary>
        /// get: retrive InternationalCode
        /// set: set InternationalCode
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "InternationalCode is required")]
        [StringLength(maximumLength: 3, ErrorMessage = "Invalid InternationalCode (Min:1 char, Max:3 char)", MinimumLength = 1)]
        public string InternationalCode { get; set; }

        /// <summary>
        /// get: retrive ContactNumber
        /// set: set ContactNumber
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "ContactNumber is required")]
        [StringLength(maximumLength: 15, ErrorMessage = "Invalid ContactNumber (Min:10 char, Max:15 char)", MinimumLength = 10)]
        public string ContactNumber { get; set; }

        /// <summary>
        /// get: retrive ContactNumber
        /// set: set ContactNumber
        /// </summary>
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Email is required")]
        [StringLength(maximumLength: 50, ErrorMessage = "Invalid Email (Min:10 char, Max:50 char)", MinimumLength = 10)]
        [RegularExpression(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$",ErrorMessage = "Invalid Email")]
        public string Email { get; set; }
    }

    /// <summary>
    /// Component table activation model
    /// </summary>
    public class ComponentActivation:ActivationModel
    {
        /// <summary>
        /// get: retrive ComponentId
        /// set: set ComponentId
        /// </summary>
        [Required(ErrorMessage = "ComponentId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "ComponentId should be greater than 0 (zero)")]
        public int ComponentId { get; set; }
    }

    /// <summary>
    /// Component table activation model
    /// </summary>
    public class TicketCancelModel : TicketAckModel
    {
        /// <summary>
        /// get: retrive CancelNote
        /// set: set CancelNote
        /// </summary>
        [Required(ErrorMessage = "CancelNote is required")]
        [StringLength(200, ErrorMessage = "Invalid Cancel Note (Max 200 char,Min 10 char)",MinimumLength =10)]
        public string CancelNote { get; set; }
    }

    /// <summary>
    /// Component table activation model
    /// </summary>
    public class FeedbackActivation : ActivationModel
    {
        /// <summary>
        /// get: retrive TicketId
        /// set: set TicketId
        /// </summary>
        [Required(ErrorMessage = "TicketId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "TicketId should be greater than 0 (zero)")]
        public int TicketId { get; set; }
    }

    /// <summary>
    /// Component table activation model
    /// </summary>
    public class UnitActivation : ActivationModel
    {
        /// <summary>
        /// get: retrive UnitId
        /// set: set UnitId
        /// </summary>
        [Required(ErrorMessage = "UnitId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "UnitId should be greater than 0 (zero)")]
        public int UnitId { get; set; }
    } 

    /// <summary>
      /// Component table activation model
      /// </summary>
    public class TenantActivation : ActivationModel
    {
        /// <summary>
        /// get: retrive TenantId
        /// set: set TenantId
        /// </summary>
        [Required(ErrorMessage = "TenantId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "TenantId should be greater than 0 (zero)")]
        public int TenantId { get; set; }
    } 
    
    /// <summary>
      /// Component table activation model
      /// </summary>
    public class LoginActivation : ActivationModel
    {
        /// <summary>
        /// get: retrive TenantId
        /// set: set TenantId
        /// </summary>
        [Required(ErrorMessage = "TenantId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "TenantId should be greater than 0 (zero)")]
        public int TenantId { get; set; }
    } 
    
    /// <summary>
      /// Component table activation model
      /// </summary>
    public class ClientActivation : ActivationModel
    {
        /// <summary>
        /// get: retrive ClientId
        /// set: set ClientId
        /// </summary>
        [Required(ErrorMessage = "ClientId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "ClientId should be greater than 0 (zero)")]
        public int ClientId { get; set; }
    }
   
    /// <summary>
    /// RequestTypeMaster table scheme
    /// </summary>
    public class RequestActivationModel: ClientIdModel
    {
        /// <summary>
        /// get: retrive RequestTypeId
        /// set: set RequestTypeId
        /// </summary>
        [Required(ErrorMessage = "RequestTypeId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "RequestTypeId should be greater than 0 (zero)")]
        public int RequestTypeId { get; set; }       

        /// <summary>
        /// get: retrive ClientId
        /// set: set ClientId
        /// </summary>
        //[Required(ErrorMessage = "ClientId is required")]
        //[Range(1, int.MaxValue, ErrorMessage = "ClientId should be greater than 0 (zero)")]
        //public int ClientId { get; set; }

        /// <summary>
        /// get: retrive IsActive
        /// set: set IsActive
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Invalid IsActive Status")]
        public bool IsActive { get; set; }
    }

    /// <summary>
    /// RequestTypeMaster table scheme
    /// </summary>
    public class RequestTypeModel: RequestActivationModel
    {
        /// <summary>
        /// get: retrive RequestType
        /// set: set RequestType
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "ComponentId is required")]
        [StringLength(maximumLength:100,MinimumLength =3,ErrorMessage ="Invalid Request Type")]
        public string RequestType { get; set; }
    }

    /// <summary>
    /// Units table scheme
    /// </summary>
    public class UnitModel: ClientIdModel
    {
        /// <summary>
        /// get:Retrive Unit id
        /// set:Set unit Id
        /// </summary>
        [Required(ErrorMessage = "UnitId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "UnitId should be greater than 0 (zero)")]
        [Key]
        public int UnitId { get; set; }

        /// <summary>
        /// get:Retrive ClientID
        /// set:Set ClientID
        /// </summary>
        //[Required(ErrorMessage = "ClientID is required")]
        //[Range(1, int.MaxValue, ErrorMessage = "ClientID should be greater than 0 (zero)")]
        //public int ClientID { get; set; }

        /// <summary>
        /// get:Retrive TenantId
        /// set:Set TenantId
        /// </summary>
        [Required(ErrorMessage = "TenantId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "TenantId should be greater than 0 (zero)")]
        public int TenantId { get; set; }

        /// <summary>
        /// get:Retrive ComponentId
        /// set:Set ComponentId
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "ComponentId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "ComponentId should be greater than 0 (zero)")]
        public int ComponentId { get; set; }

        /// <summary>
        /// get:Retrive UnitNumber
        /// set:Set UnitNumber
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "Unit Number is required")]
        [StringLength(maximumLength: 40, ErrorMessage = "Invalid Unit Number (Min:1, Max:40)", MinimumLength = 1)]
        public string UnitNumber { get; set; }

        /// <summary>
        /// get:Retrive TenancyStart
        /// set:Set TenancyStart
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "TenancyStart is required")]
        public DateTime TenancyStart { get; set; }

        /// <summary>
        /// get:Retrive TenancyEnd
        /// set:Set TenancyEnd
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "TenancyEnd is required")]
        public DateTime TenancyEnd { get; set; }

        /// <summary>
        /// get:Retrive ComponentNumber
        /// set:Set ComponentNumber
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "Component Number is required")]
        [StringLength(maximumLength: 80, ErrorMessage = "Invalid Component Number (Min:1, Max:80)", MinimumLength = 1)]
        public string ComponentNumber { get; set; }
    }

    /// <summary>
    /// Tenant table scheme
    /// </summary>
    public class TenantModel:ClientIdModel
    {
        /// <summary>
        /// get:Retrive TenantId
        /// set:Set TenantId
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "TenantId is required")]
        [Key]
        [Range(1, int.MaxValue, ErrorMessage = "TenantId should be greater than 0 (zero)")]
        public int TenantId { get; set; }

        /// <summary>
        /// get:Retrive ClientID
        /// set:Set ClientID
        /// </summary>
        //[Required(AllowEmptyStrings =false, ErrorMessage = "ClientID is required")]
        //[Range(1, int.MaxValue, ErrorMessage = "ClientID should be greater than 0 (zero)")]
        //public int ClientID { get; set; }

        /// <summary>
        /// get:Retrive TenantNumber
        /// set:Set TenantNumber
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "Tenant Number is required")]
        [StringLength(maximumLength: 20, ErrorMessage = "Invalid Tenant Number  (Min:1, Max:20)", MinimumLength = 1)]
        public string TenantNumber { get; set; }

        /// <summary>
        /// get:Retrive TenantName
        /// set:Set TenantName
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "Tenant Name is required")]
        [StringLength(maximumLength: 100, ErrorMessage = "Invalid Tenant Name  (Min:3, Max:100)", MinimumLength = 3)]
        public string TenantName { get; set; }

        /// <summary>
        /// get:Retrive Age
        /// set:Set Age
        /// </summary>
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Age is required")]
        [Range(0, 130, ErrorMessage = "Age range 0 - 130 Years")]
        public int Age { get; set; }


        /// <summary>
        /// get:Retrive Gender value
        /// set:Set Gender Value
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "Gender is required")]
        [StringLength(maximumLength: 1, ErrorMessage = "Invalid Gender  (1 char - M/F)", MinimumLength = 1)]
        [RegularExpression(@"(?i)^[M,F,m,f]+",ErrorMessage ="Invalid Gender(1 char - M / F)")]
        public string Sex { get; set; }

        /// <summary>
        /// get:Retrive Address
        /// set:Set Address
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "Address is required")]
        [StringLength(maximumLength: 200, ErrorMessage = "Invalid Address  (Min:3, Max:200)", MinimumLength = 3)]
        public string Address { get; set; }

        /// <summary>
        /// get:Retrive Nationality
        /// set:Set Nationality
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "Nationality is required")]
        [StringLength(maximumLength: 100, ErrorMessage = "Invalid Nationality  (Min:3, Max:100)", MinimumLength = 3)]
        public string Nationality { get; set; }

        /// <summary>
        /// get:Retrive City
        /// set:Set City
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "City is required")]
        [StringLength(maximumLength: 100, ErrorMessage = "Invalid City  (Min:1, Max:100)", MinimumLength = 3)]
        public string City { get; set; }
    }

    /// <summary>
    /// Change Status scheme
    /// </summary>
    public class ChangeStatusModel
    {
        /// <summary>
        /// get:Retrive TenantId
        /// set:Set TenantId
        /// </summary>
        //[Required(AllowEmptyStrings =false, ErrorMessage = "TenantId is required")]
        //[Range(1, int.MaxValue, ErrorMessage = "TenantId should be greater than 0 (zero)")]
        //[Key]
        //public int TenantId { get; set; }

        /// <summary>
        /// get:Retrive ClientID
        /// set:Set ClientID
        /// </summary>
        //[Required(AllowEmptyStrings =false, ErrorMessage = "ClientID is required")]
        //[Range(1, int.MaxValue, ErrorMessage = "ClientID should be greater than 0 (zero)")]
        //public int ClientID { get; set; }

        /// <summary>
        /// get:Retrive TicketId
        /// set:Set TicketId
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "TicketNo is required")]
        [StringLength(maximumLength:10,MinimumLength =4, ErrorMessage = "TicketNo should be min 4 chars")]
        public string TicketNo { get; set; }

        /// <summary>
        /// get:Retrive StatusId
        /// set:Set StatusId
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "StatusId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "StatusId should be greater than 0 (zero)")]
        public int StatusId { get; set; }
    }

    /// <summary>
    /// Tenant Lease scheme
    /// </summary>
    public class TenantLeaseModel
    {
        /// <summary>
        /// get:Retrive TenantId
        /// set:Set TenantId
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "UnitId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "UnitId should be greater than 0 (zero)")]
        [Key]
        public int UnitId { get; set; }


        /// <summary>
        /// get:Retrive TenantId
        /// set:Set TenantId
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "TenantId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "TenantId should be greater than 0 (zero)")]
        public int TenantId { get; set; }

        /// <summary>
        /// get:Retrive TenancyStart
        /// set:Set TenancyStart
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "TenancyStart is required")]
        public DateTime TenancyStart { get; set; }

        /// <summary>
        /// get:Retrive TenancyEnd
        /// set:Set TenancyEnd
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "TenancyEnd is required")]
        public DateTime TenancyEnd { get; set; }
    }

    /// <summary>
    /// Service Ack model
    /// </summary>
    public class ServiceAckModel
    {
        /// <summary>
        /// get:Retrive feedbackId
        /// set:Set feedbackId
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "TicketNo is required")]
        [StringLength(maximumLength:10,MinimumLength =4, ErrorMessage = "TicketNo should be min 4 chars")]
        [Key]
        public string TicketNo { get; set; }
    }

    /// <summary>
    /// ServiceFeedBack Table Scheme
    /// </summary>
    public class ServiceFeedBackModel
    {
        ///// <summary>
        ///// get:Retrive ClientId
        ///// set:Set ClientId
        ///// </summary>
        //[Required(ErrorMessage = "ClientId should be positive integer", ErrorMessageResourceName = "ClientId")]
        //[Range(1, int.MaxValue, ErrorMessage = "ClientId should be greater than 0 (zero)")]
        //public int ClientId { get; set; }

        /// <summary>
        /// get:Retrive TicketId
        /// set:Set TicketId
        /// </summary>
        [Required(ErrorMessage = "TicketId should be positive integer",ErrorMessageResourceName ="TicketId")]
        [Range(1,int.MaxValue, ErrorMessage = "TicketId should be greater than 0 (zero)")]
        [Key]
        public int TicketId { get; set; }

        /// <summary>
        /// get:Retrive FeedBack
        /// set:Set FeedBack
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "ComponentId is required")]
        [StringLength(maximumLength:200,MinimumLength =5,ErrorMessage ="Invalid Feedback length (Max=200 Char,Min=5)")]
        public string FeedBack { get; set; }

        /// <summary>
        /// get:Retrive FeedBack
        /// set:Set FeedBack
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "ComponentId is required")]
        [Range(0, 5, ErrorMessage = "Please enter valid integer Number(1-5) for Rating")]
        [Display(Name ="Rating")]
        public int Rating { get; set; }
    }

    /// <summary>
    /// Ticket Ack model
    /// </summary>
    public class TicketAckModel
    {
        /// <summary>
        /// get:Retrive feedbackId
        /// </summary>
        [Required(AllowEmptyStrings =false, ErrorMessage = "TicketNo is required")]
        [StringLength(maximumLength:10, MinimumLength =4, ErrorMessage = "Please enter valid TicketNo (min 4 chars)")]
        [Key]
        public string TicketNo { get; set; }
    }

    /// <summary>
    /// Ticket Table scheme
    /// </summary>
    public class TicketModel:ClientIdModel
    {
        /// <summary>
        /// Get: Retrive TenantId
        /// Set: Set TenantId
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "TenantId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "TenantId should be greater than 0 (zero)")]
        public int TenantId { get; set; }

        /// <summary>
        /// Get: Retrive LocationId
        /// Set: Set LocationId
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "LocationId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "LocationId should be greater than 0 (zero)")]
        public int LocationId { get; set; }

        /// <summary>
        /// Get: Retrive ClientId
        /// Set: Set ClientId
        /// </summary>
        //[Required(AllowEmptyStrings = false, ErrorMessage = "ClientId is required")]
        //[Range(1, int.MaxValue, ErrorMessage = "ClientId should be greater than 0 (zero)")]
        //public int ClientId { get; set; }

        /// <summary>
        /// Get: Retrive TicketNo
        /// Set: Set TicketNo
        /// </summary>
        //[Required(AllowEmptyStrings = false, ErrorMessage = "Feedback is required")]
        //[StringLength(maximumLength: 20, ErrorMessage = "Max length of TicketNo is 20 charectors")]
        public String TicketNo { get; set; }

        /// <summary>
        /// Get: Retrive RequestTypeId
        /// Set: Set RequestTypeId
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "RequestTypeId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "RequestTypeId should be greater than 0 (zero)")]
        public int RequestTypeId { get; set; }

        /// <summary>
        /// Get: Retrive RequestTitle
        /// Set: Set RequestTitle
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "RequestTitle is required")]
        [StringLength(maximumLength: 100,MinimumLength =5, ErrorMessage = "Max length of RequestTitle is 100 charectors and min is 5 char")]
        public String RequestTitle { get; set; }

        /// <summary>
        /// Get: Retrive RequestDecription
        /// Set: Set RequestDecription
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "RequestDescription is required")]
        [StringLength(maximumLength: 500,MinimumLength =5, ErrorMessage = "Max length of RequestDescription is 500 charectors and min is 5")]
        public String RequestDescription { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "Priority is required")]
        [StringLength(maximumLength: 10, MinimumLength = 1, ErrorMessage = "Max length of Priority is 10 charectors")]
        public String Priority { get; set; } = "Normal";

        /// <summary>
        /// Get: Retrive StatusId
        /// Set: Set StatusId
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "StatusId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "StatusId should be greater than 0 (zero)")]
        public int StatusId { get; set; }

        /// <summary>
        /// Get: Retrive CancelNote
        /// Set: Set CancelNote
        /// </summary>
        [StringLength(maximumLength: 200, MinimumLength = 0, ErrorMessage = "Max length of Priority is 200 charectors")]
        public String CancelNote { get; set; }

        /// <summary>
        /// Get: Retrive LocationType
        /// Set: Set LocationType
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "LocationType is required")]
        [Range(1, 2, ErrorMessage = "invalid LocationType,accept 1 for Common Area and 2 for Unit/Flat")]
        public int LocationType { get; set; }

        /// <summary>
        /// Get: Retrive IsActive
        /// Set: Set IsActive
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "IsActive is required")]
        public bool IsActive { get; set; }
    }

    /// <summary>
    /// Batch update of Tenant and respective master
    /// </summary>
    public class TenantBatchModel
    {
        public List<TenantModel> TenantCollection { get; set; }
        public List<LoginModel> LoginCollection { get; set; }
        public List<UnitModel> UnitCollection { get; set; }
        public List<ComponentModel> ComponentCollection { get; set; }
        public List<ClientModel> ClientCollection { get; set; }
    }

    public class Notifications
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "RegToken is required")]
        [StringLength(500, ErrorMessage = "Invalid RegToken (Min: 64 char,Max:500 char", MinimumLength = 1)]
        public string RegToken { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "NotifyTitle is required")]
        [StringLength(100, ErrorMessage = "Invalid NotifyTitle (Min: 5 char,Max:100 char", MinimumLength = 5)]
        public string NotifyTitle { get; set; }

        [StringLength(100, ErrorMessage = "Invalid NotifyMsg (Min: 5 char,Max:100 char", MinimumLength = 5)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "NotifyMsg is required")]
        public string NotifyMsg { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "NotifySoundEnable is required")]
        public bool NotifySoundEnable { get; set; }
    }

    public class NotificationData
    {
        public string NotificationType { get; set; }
        public int TicketId { get; set; }
        public DateTime TenancyStartDate { get; set; }
        public DateTime TenancyEndDate { get; set; }
        public int ClientId { get; set; }
    }

    public enum NotificationType
    {
        TicketStatus = 1,
        LeaseUpdate = 2
    }

    public class AndroidNotify
    {
        public string registration_ids { get; set; }
        public NotificationData data { get; set; }
        public string priority { get; set; }
        public string Title { get; set; }

    }

    public class iOSNotify:AndroidNotify
    {
        public string notification { get; set; }
    }

    public enum TicketStatus
    {
        Sent=1,
        Process=2,
        InProgress,
        Close,
        Cancelled
    }

    public enum DeviceType
    {
        Android=1,
        iOS=2,
        Windows=3
    }
}