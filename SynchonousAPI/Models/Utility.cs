﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Xml.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Xml;
using System.Linq;
using System.Net;
using System.Web.Script.Serialization;

namespace SynchonousAPI.Models
{
    public static class Utility
    {
        public static string Decode64String(string encodeValue)
        {
            try
            {
                byte[] data = Convert.FromBase64String(encodeValue);
                return Encoding.UTF8.GetString(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA256.Create();  //or use SHA256.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
            {
                sb.Append(b.ToString("X2"));
            }

            return sb.ToString();
        }

        public static bool CompairAPIKey(string apiKey,string clientId)
        {
            if (!string.IsNullOrEmpty(apiKey))
            {
                try
                {
                    string fileName = "ApiKeyConfig.xml";
                    var filePath = System.Web.Hosting.HostingEnvironment.MapPath("~/" + fileName);
                    string xmlFile = File.ReadAllText(filePath);
                    XmlDocument xmldoc = new XmlDocument();
                    xmldoc.LoadXml(xmlFile);
                    XmlNodeList nodeList = xmldoc.SelectNodes("ApiKeys/ApiKey");
                    string Short_Fall = string.Empty;
                    return nodeList.Cast<XmlNode>().Any(x => x.InnerText.ToLower() == apiKey.ToLower() && x.Attributes["ClientId"].Value.ToString().Equals(clientId));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            else
                return false;
        }

        public static string sendNotification<T>(string RegToken, T model, string title, string body)
        {
            String sResponseFromServer = string.Empty;
            try
            {               
                string applicationID = "AAAAlxAM8qY:APA91bHE2a9AP8XPg2zGxu5FGlJRmo43eBm6_nuccYAawZGY0BigaIZJ21yzq9FupJPxjWbg2y0rGGdB8cd4OrMTU_Kgf2MqH_W70vGR_qIKYI_DPEqgaM98wnXB7_rGQUkE1Jz6rBm9";

                string senderId = "648809345702";
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new
                {
                    notification = new
                    {
                        title = title,
                        body = body,
                    },
                    data = model,
                    to = RegToken
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                 sResponseFromServer = tReader.ReadToEnd();
                                
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }
            return sResponseFromServer;
        }
    }
}