﻿namespace SynchonousAPI.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Web.Http;
    using DataModel;
    using SynchonousAPI.Filter;
    using SynchonousAPI.Models;
    using SynchronousAPI.Models;
    using System.Web.Http.ModelBinding;
    using System.Data.SqlClient;

    /// <summary>
    /// it contain Synchronous push API methods
    /// </summary>
    [ValidateAPIKey]
    [RoutePrefix("api/masters")]
    public class MastersController : ApiController
    {
        /// <summary>
        /// To insert and update Login details
        /// </summary>
        /// <param name="requestTypeModel">LoginModel</param>
        /// <returns>no of inserted and updated record</returns>
        [HttpPost]
        [Route("RequestTypeDetails")]
        public HttpResponseMessage RequestTypeDetails([FromBody] List<RequestTypeModel> requestTypeModel)
        {
            //// Validation Check
            if (requestTypeModel != null && ModelState.IsValid && requestTypeModel.Count > 0)
            {
                MaisonetteEntities DbContext = null;
                try
                {
                    //// To check No. of Distinct Record in the collection
                    int distinctCount = requestTypeModel.GroupBy(column => new { column.ClientId, column.RequestTypeId }).Select(g => g.First()).ToList().Count();

                    //// Ensure that there is no redunded record exist in the collection
                    if (distinctCount == requestTypeModel.Count)
                    {
                        DbContext = new MaisonetteEntities();
                        DbContext.Configuration.AutoDetectChangesEnabled = false;
                        //// Hold the updated Record value
                        int count = 0;
                        foreach (var requestTypeModelColumn in requestTypeModel)
                        {
                            //// fetcting  record from db if exists
                            var RequestTypeTableRow = DbContext.RequestTypeMasters.Where(requestTypeColumn => requestTypeColumn.ClientId.Equals(requestTypeModelColumn.ClientId)
                                                                       && requestTypeColumn.RequestTypeId.Equals(requestTypeModelColumn.RequestTypeId)
                                                                      ).FirstOrDefault();
                            //// if  there is no record then insert  
                            if (RequestTypeTableRow == null)
                            {
                                RequestTypeMaster requestType = new RequestTypeMaster();
                                requestType.ClientId = requestTypeModelColumn.ClientId;
                                requestType.RequestType = requestTypeModelColumn.RequestType;
                                requestType.RequestTypeId = requestTypeModelColumn.RequestTypeId;
                                requestType.CreationDate = DateTime.Now;
                                requestType.ModificationDate = DateTime.Now;
                                requestType.IsActive = true;
                                ++count;
                                DbContext = InsertBulk.AddToContext<RequestTypeMaster>(DbContext, requestType, count, 100, true);
                            }
                            else
                            {
                                //// if  there is any record then Update 
                                RequestTypeTableRow.RequestType = requestTypeModelColumn.RequestType;
                                RequestTypeTableRow.ModificationDate = DateTime.Now;
                                DbContext.Entry(RequestTypeTableRow).State = EntityState.Modified;
                            }
                        }
                        //// commit data into database
                        DbContext.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Record Changed", new { RecordInserted = count, RecordUpdated = distinctCount - count }));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Some duplicate set of {ClientId,RequestTypeId}  exists in supplied Collection", null));
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
                finally
                {
                    if (DbContext != null)
                    {
                        DbContext.Dispose();
                    }
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
            }
        }

        /// <summary>
        /// To Activate and Deactivate Request Type details
        /// </summary>
        /// <param name="requestTypeCollection">RequestTypeModel</param>
        /// <returns>Activation status</returns>
        [HttpPost]
        [Route("RequestTypeActivation")]
        public HttpResponseMessage RequestTypeActivation([FromBody] List<RequestActivationModel> requestTypeCollection)
        {
            if (requestTypeCollection != null && ModelState.IsValid)
            {
                try
                {
                    using (MaisonetteEntities dbEntity = new MaisonetteEntities())
                    {
                        foreach (RequestActivationModel collectionItem in requestTypeCollection)
                        {
                            var currentRequestType = dbEntity.RequestTypeMasters.Where(columnSet => columnSet.ClientId.Equals(collectionItem.ClientId) && columnSet.Id.Equals(collectionItem.RequestTypeId)).FirstOrDefault();
                            if (currentRequestType != null)
                            {
                                currentRequestType.IsActive = collectionItem.IsActive;
                                currentRequestType.ModificationDate = DateTime.Now;
                                dbEntity.Entry(currentRequestType).State = EntityState.Modified;
                            }
                        }

                        int updatedRecord = dbEntity.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Record Updated", new { UpdatedRecord = updatedRecord }));
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Invalid ClientId/RequestTypeId/IsActive in collection", GetErrorList()));
            }
        }

        /// <summary>
        /// To Update multiple ticket status
        /// </summary>
        /// <param name="model">ChangeStatusModel</param>
        /// <returns>no of updated record</returns>
        [HttpPost]
        [Route("UpdateTicketStatus")]
        public HttpResponseMessage TicketChangeStatus([FromBody] List<ChangeStatusModel> model)
        {
            //// Validation Check
            if (model != null && ModelState.IsValid && model.Count > 0)
            {
                MaisonetteEntities context = null;
                try
                {
                    context = new MaisonetteEntities();
                    context.Configuration.AutoDetectChangesEnabled = false;
                    //// Hold the updated Record value
                    int updatedRecord = 0;
                    int ClientId = getHeaderClientId();
                    List<string> InvalidStatusId = new List<string>();
                    List<string> AlreadyUpdatedTicket = new List<string>();
                    List<string> notify = new List<string>();
                    foreach (var currentItem in model)
                    {
                        var ticketStatus = context.TicketStatusMasters.Where(x =>x.Id.Equals(currentItem.StatusId) && x.IsActive).FirstOrDefault();
                        if (ticketStatus != null)
                        {
                            //// fetcting  record from db if exists
                            var rowSet = context.Tickets.Where(x => x.ClientId.Equals(ClientId) && x.TicketNo.Equals(currentItem.TicketNo) && x.IsActive==true).FirstOrDefault();
                            //// if  there is record then update Status  
                            if (rowSet != null)
                            {
                                if (rowSet.StatusId== currentItem.StatusId)
                                {
                                    AlreadyUpdatedTicket.Add(currentItem.TicketNo);
                                }
                                else
                                {
                                    rowSet.ModificationDate = DateTime.Now;
                                    rowSet.IsSync = false;
                                    rowSet.StatusId = currentItem.StatusId;
                                   if(currentItem.StatusId==(int)TicketStatus.Cancelled)
                                    {
                                        rowSet.IsCancelSync = false;
                                        rowSet.CancellationDate = DateTime.Now;
                                    }
                                    context.Entry(rowSet).State = EntityState.Modified;

                                    #region Send Push Notification
                                    var login = context.Logins.Where(x => x.TenantId.Equals(rowSet.TenantId) && x.ClientId.Equals(ClientId)).FirstOrDefault();
                                    if (login != null)
                                    {
                                        if (!string.IsNullOrEmpty(login.DeviceRegToken))
                                        {
                                            NotificationData data = new NotificationData();
                                            data.NotificationType = NotificationType.TicketStatus.ToString();
                                            data.TicketId = rowSet.Id;
                                            data.TenancyStartDate = DateTime.MinValue;
                                            data.TenancyEndDate = DateTime.MinValue;
                                            data.ClientId = login.ClientId;
                                            string title = "Ticket No. " + rowSet.TicketNo + " status has been changed";
                                            AndroidNotify androidModel = new AndroidNotify();
                                            androidModel.registration_ids = login.DeviceRegToken;
                                            androidModel.data = data;
                                            androidModel.priority = "High";
                                            androidModel.Title = title;

                                            iOSNotify iOsModel = new iOSNotify();
                                            iOsModel.notification = "Ios";
                                            iOsModel.priority = "High";
                                            iOsModel.registration_ids = login.DeviceRegToken;
                                            iOsModel.data = data;
                                            iOsModel.Title = title;

                                            if (login.DeviceType.ToLower() == DeviceType.Android.ToString().ToLower())
                                            {
                                                string str = Utility.sendNotification(login.DeviceRegToken, androidModel, "Ticket Update", "Ticket No. " + rowSet.TicketNo + " status has been changed");
                                                notify.Add(str);
                                            }
                                            else if (login.DeviceType.ToLower() == DeviceType.iOS.ToString().ToLower())
                                            {
                                                string str = Utility.sendNotification(login.DeviceRegToken, iOsModel, "Ticket Update", "Ticket NO " + rowSet.TicketNo + " status has been changed");
                                                notify.Add(str);
                                            }
                                        }
                                    }
                                    #endregion 
                                }                                
                            }
                        }
                        else
                        {
                            InvalidStatusId.Add(currentItem.StatusId + " is an invalid status id");
                        }
                    }
                    //// commit data into database
                    updatedRecord = context.SaveChanges();
                    if (updatedRecord > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Record Modified", new { RecordUpdated = updatedRecord + " out of " + model.Count,NotifyStatus=notify, InvalidStatusIds = InvalidStatusId,AlreadyUpdatedTicket=AlreadyUpdatedTicket }));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Either no record found or Invalid status is/Already updated", new { RecordUpdated = updatedRecord + " out of " + model.Count, InvalidStatusIds = InvalidStatusId, AlreadyUpdatedTicket = AlreadyUpdatedTicket }));
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
                finally
                {
                    if (context != null)
                    {
                        context.Dispose();
                    }
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
            }
        }

        /// <summary>
        /// get service Tickets retrival acknowledgement from client and mark sync in our database 
        /// </summary>
        /// <returns>no of updated record</returns>
        [HttpPost]
        [Route("TicketsAck")]
        public HttpResponseMessage TicketsAck([FromBody] List<TicketAckModel> model)
        {
            //// Validation Check
            if (model != null && ModelState.IsValid && model.Count > 0)
            {
                MaisonetteEntities context = null;
                try
                {
                    context = new MaisonetteEntities();
                    context.Configuration.AutoDetectChangesEnabled = false;
                    //// Hold the updated Record value
                    int updatedRecord = 0;
                    int ClientId = getHeaderClientId();
                    List<string> InvalidTicketNo = new List<string>();
                    List<string> AlreadySyncTicketNo = new List<string>();
                    foreach (var item in model)
                    {
                        //// fetcting  record from db if exists
                        var rowSet = context.Tickets.Where(x => x.TicketNo.Equals(item.TicketNo) && x.ClientId.Equals(ClientId) && x.IsActive==true).FirstOrDefault();
                        //// if  there is no record then insert  
                        if (rowSet != null)
                        {
                            if (!rowSet.IsSync)
                            {
                                rowSet.IsSync = true;
                                rowSet.ModificationDate = DateTime.Now;
                                if (rowSet.StatusId == (int)TicketStatus.Cancelled && rowSet.IsCancelSync == false) // 
                                {
                                    rowSet.IsCancelSync = true;
                                }
                                context.Entry(rowSet).State = EntityState.Modified;
                            }
                            else
                            {
                                AlreadySyncTicketNo.Add(item.TicketNo);
                            }
                        }
                        else
                        {
                            InvalidTicketNo.Add(item.TicketNo);
                        }
                    }
                    //// commit data into database
                    updatedRecord = context.SaveChanges();
                    if(updatedRecord>0)
                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Record Changed", new { RecordUpdated = updatedRecord + " out of " + model.Count, InvalidTicketNo = InvalidTicketNo, AlreadySyncTicketNo= AlreadySyncTicketNo }));
                    else
                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Record not found", new { RecordUpdated = updatedRecord + " out of " + model.Count, InvalidTicketNo = InvalidTicketNo, AlreadySyncTicketNo = AlreadySyncTicketNo }));
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
                finally
                {
                    if (context != null)
                    {
                        context.Dispose();
                    }
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
            }
        }

        /// <summary>
        /// To insert Ticket details
        /// </summary>
        /// <param name="ticketsInput">TicketModel</param>
        /// <returns>no. of inserted and updated record</returns>
        [HttpPost]
        [Route("CreateTicket")]
        public HttpResponseMessage CreateTicket([FromBody] List<TicketModel> ticketsInput)
        {
            //// Validation Check
            if (ticketsInput != null && ModelState.IsValid && ticketsInput.Count > 0)
            {
                MaisonetteEntities context = null;
                try
                {
                   
                   
                        context = new MaisonetteEntities();
                        context.Configuration.AutoDetectChangesEnabled = false;
                        int newTicketCount = 0;
                        List<string> existTicketNo = new List<string>();
                        List<int> newTicketId = new List<int>();
                        foreach (var currentIpnut in ticketsInput)
                        {
                            //// fetcting  record from db if exists
                            //var TicketTableRow = context.Tickets.Where(column => column.TicketNo.Equals(currentIpnut.TicketNo)).FirstOrDefault();
                            //// if  there is no record then insert  
                            //if (TicketTableRow == null)
                            //{
                                Ticket newTicket = new Ticket();
                                newTicket.ClientId = currentIpnut.ClientId;
                                newTicket.TicketNo = "";
                                newTicket.TenantId = currentIpnut.TenantId;
                                newTicket.CreationDate = DateTime.Now;
                                newTicket.ModificationDate = DateTime.Now;
                                newTicket.IsSync = false;
                                newTicket.RequestDecription = currentIpnut.RequestDescription;
                                newTicket.RequestTitle = currentIpnut.RequestTitle;
                                newTicket.RequestTypeId = currentIpnut.RequestTypeId;
                                newTicket.Priority = currentIpnut.Priority;
                                newTicket.StatusId = currentIpnut.StatusId;
                                newTicket.CancelNote = currentIpnut.CancelNote;
                                newTicket.LocationType = currentIpnut.LocationType;
                                newTicket.IsActive = currentIpnut.IsActive;
                                newTicket.LocationId = currentIpnut.LocationId;
                                newTicket.IsActive = true;
                                context = InsertBulk.AddToContext<Ticket>(context, newTicket, newTicketCount, 100, true);
                                context.Tickets.Add(newTicket);
                                context.SaveChanges();

                                //Adding New Created Ticket Id into list
                                newTicketId.Add(newTicket.Id);

                                var createdTicket = context.Tickets.Where(column =>column.Id.Equals(newTicket.Id)).FirstOrDefault();
                                createdTicket.TicketNo = "TIC" + newTicket.Id;
                                context.Entry(createdTicket).State = EntityState.Modified;
                                context.SaveChanges();
                                newTicketCount += 1;
                            //}
                            //else
                            //{
                            //    existTicketNo.Add(currentIpnut.TicketNo);
                            //}
                        }
                        //// commit data into database
                        //context.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordInserted, "Record Inserted", new { RecordInserted = newTicketCount, TicketAlreadyExist = existTicketNo,NewTicketId=newTicketId }));                   
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
                finally
                {
                    if (context != null)
                    {
                        context.Dispose();
                    }
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
            }
        }

        /// <summary>
        /// get service Feedback retrival acknowledgement from client and mark sync in our database 
        /// </summary>
        /// <param name="model">ServiceAckModel</param>
        /// <returns></returns>
        [HttpPost]
        [Route("servicefeedbackack")]
        public HttpResponseMessage ServiceFeedbackAck([FromBody] List<ServiceAckModel> model)
        {
            //// Validation Check
            if (model != null && ModelState.IsValid && model.Count > 0)
            {
                int ClientId = getHeaderClientId();
                MaisonetteEntities context = null;
                try
                {
                    context = new MaisonetteEntities();
                    context.Configuration.AutoDetectChangesEnabled = false;
                    //// Hold the updated Record value
                    int updatedRecord = 0;
                    List<string> AlreadySyncTicketNo = new List<string>();
                    List<string> InvalidTicketNo = new List<string>();
                    foreach (var item in model)
                    {
                        var ticketRowSet = context.Tickets.Where(x => x.TicketNo.Equals(item.TicketNo) && x.ClientId.Equals(ClientId) && x.IsActive).FirstOrDefault();
                        if (ticketRowSet != null)
                        {
                            var rowSet = context.ServiceFeedbacks.Where(x => x.TicketID.Equals(ticketRowSet.Id) && x.ClientID.Equals(ClientId) && x.IsActive).FirstOrDefault();
                            //// if  there is no record then insert  
                            if (rowSet != null)
                            {
                                if (!rowSet.IsSync)
                                {
                                    rowSet.IsSync = true;
                                    rowSet.ModificationDate = DateTime.Now;
                                    context.Entry(rowSet).State = EntityState.Modified;
                                }
                                else
                                {
                                    AlreadySyncTicketNo.Add(item.TicketNo);
                                }
                            }
                            else
                            {
                                InvalidTicketNo.Add(item.TicketNo);
                            }
                        }
                        else
                        {
                            InvalidTicketNo.Add(item.TicketNo);
                        }
                    }
                    //// commit data into database
                    updatedRecord = context.SaveChanges();
                    if (updatedRecord > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Record Modified", new { RecordUpdated = updatedRecord + " out of " + model.Count, InvalidTicketNo = InvalidTicketNo, AlreadySyncTicketNo = AlreadySyncTicketNo }));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Record not Modified", new { RecordUpdated = updatedRecord + " out of " + model.Count, InvalidTicketNo = InvalidTicketNo, AlreadySyncTicketNo = AlreadySyncTicketNo }));
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
                finally
                {
                    if (context != null)
                    {
                        context.Dispose();
                    }
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
            }
        }

        /// <summary>
        /// To Update multiple tenant lease period
        /// </summary>
        /// <returns>Return all service feedback record which is still not sync</returns>
        [HttpGet]
        [Route("GetFeedback")]
        public HttpResponseMessage GetServiceFeedback()
        {
            try
            {
                int ClientId = getHeaderClientId();
                using (MaisonetteEntities context = new MaisonetteEntities())
                {
                    context.Configuration.ProxyCreationEnabled = false;
                    var result = from feed in context.ServiceFeedbacks
                                 join ticket in context.Tickets on feed.TicketID equals ticket.Id
                                 where feed.IsSync==false && feed.IsActive==true && feed.ClientID==ClientId
                                 select new {
                                     feed.FeedbackId,
                                     feed.ClientID,
                                     feed.Feedback,
                                     feed.Rating,
                                     feed.CreationDate,
                                     feed.ModificationDate,
                                     feed.IsActive,
                                     feed.IsSync,
                                     ticket.TicketNo
    };

                    if (result != null && result.ToList().Count>0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataFound, "Record Found", result.ToList()));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Either all records has been sync or there is no active record for clientId "+ClientId, null));
                    }
                }
            }
            catch (Exception ex)
            {
                return CatchException(ex);
            }
        }

        ///// <summary>
        ///// To Activate and Deactivate Service Feedback details
        ///// </summary>
        ///// <param name="activationModel">ActivationModel</param>
        ///// <returns>Activation status</returns>
        //[HttpPost]
        //[Route("FeedbackActivation")]
        //public HttpResponseMessage FeedbackActivation([FromBody] List<FeedbackActivation> activationModel)
        //{
        //    if (activationModel != null && ModelState.IsValid && activationModel.Count > 0)
        //    {
        //        try
        //        {
        //            List<int> notUpdatelist = new List<int>();
        //            int updatedRecord = 0;
        //            using (MaisonetteEntities dbEntity = new MaisonetteEntities())
        //            {

        //                foreach (FeedbackActivation activationModelRow in activationModel)
        //                {
        //                    var feedbackTableRow = dbEntity.ServiceFeedbacks.Where(x => x.TicketID.Equals(activationModelRow.TicketId)).FirstOrDefault();
        //                    if (feedbackTableRow != null)
        //                    {
        //                        feedbackTableRow.IsActive = activationModelRow.IsActive;
        //                        feedbackTableRow.ModificationDate = DateTime.Now;
        //                        dbEntity.Entry(feedbackTableRow).State = EntityState.Modified;
        //                    }
        //                    else
        //                    {
        //                        notUpdatelist.Add(activationModelRow.TicketId);
        //                    }
        //                }
        //                updatedRecord = dbEntity.SaveChanges();
        //                if (updatedRecord > 0)
        //                {
        //                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Feedback(s) record Changed", new { UpdatedRecord = updatedRecord, TicketIdNotExist = notUpdatelist }));
        //                }
        //                else
        //                {
        //                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Feedback(s) not Exist", new { UpdatedRecord = updatedRecord, TicketIdNotExist = notUpdatelist }));
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            return CatchException(ex);
        //        }
        //    }
        //    else
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Invalid TicketId/IsActive value", GetErrorList()));
        //    }
        //}

        /// <summary>
        /// To insert and update Unit details
        /// </summary>
        /// <param name="model">UnitModel</param>
        /// <returns>no. of inserted and updated record</returns>
        [HttpPost]
        [Route("UnitDetails")]
        public HttpResponseMessage UnitDetails([FromBody] List<UnitModel> model)
        {
            //// Validation Check
            if (model != null && ModelState.IsValid && model.Count > 0)
            {
                MaisonetteEntities context = null;
                try
                {
                    //// To check No. of Distinct Record in the collection
                    int distinctCount = model.GroupBy(p => p.TenantId).Select(g => g.First()).ToList().Count();
                    //// Ensure that there is no redunded record exist in the collection
                    if (distinctCount == model.Count)
                    {
                        context = new MaisonetteEntities();
                        context.Configuration.AutoDetectChangesEnabled = false;
                        //// Hold the updated Record value
                        int count = 0;
                        foreach (var item in model)
                        {
                            //// fetcting  record from db if exists
                            var rowSet = context.Units.Where(x => x.ClientId.Equals(item.ClientId) && x.TenantId.Equals(item.TenantId)).FirstOrDefault();
                            //// if  there is no record then insert  
                            if (rowSet == null)
                            {
                                Unit unit = new Unit();
                                unit.ComponentId = item.ComponentId;
                                unit.TenancyEnd = item.TenancyEnd;
                                unit.TenancyStart = item.TenancyStart;          
                                unit.TenantId = item.TenantId;
                                unit.CreationDate = DateTime.Now;
                                unit.ModificationDate = DateTime.Now;
                                unit.IsActive = true;
                                unit.ComponentNumber = item.ComponentNumber;
                                unit.UnitId = item.UnitId;
                                unit.ClientId = item.ClientId;
                                unit.UnitNumber = item.UnitNumber;
                                ++count;
                                context = InsertBulk.AddToContext<Unit>(context, unit, count, 100, true);
                            }
                            else
                            {
                                //// if  there is any record then Update 
                                rowSet.ComponentId = item.ComponentId;
                                rowSet.TenancyEnd = item.TenancyEnd;
                                rowSet.TenancyStart = item.TenancyStart;
                                rowSet.TenantId = item.TenantId;
                                rowSet.ModificationDate = DateTime.Now;
                                rowSet.IsActive = true;
                                rowSet.ComponentNumber = item.ComponentNumber;
                                rowSet.ClientId = item.ClientId;
                                rowSet.UnitNumber = item.UnitNumber;
                                context.Entry(rowSet).State = EntityState.Modified;
                            }
                        }
                        //// commit data into database
                        context.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordInserted, "Record Modified", new { RecordInserted = count, RecordUpdated = distinctCount - count }));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Some duplicate Unit Id exists in supplied Collection", null));
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
                finally
                {
                    if (context != null)
                    {
                        context.Dispose();
                    }
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
            }
        }

        ///// <summary>
        ///// To Activate and Deactivate Unit details
        ///// </summary>
        ///// <param name="activationModel">ActivationModel</param>
        ///// <returns>Activation status</returns>
        //[HttpPost]
        //[Route("UnitActivation")]
        //public HttpResponseMessage UnitActivation([FromBody] List<UnitActivation> activationModel)
        //{
        //    if (activationModel != null && ModelState.IsValid && activationModel.Count > 0)
        //    {
        //        try
        //        {
        //            List<int> notUpdatelist = new List<int>();
        //            int updatedRecord = 0;
        //            using (MaisonetteEntities dbEntity = new MaisonetteEntities())
        //            {

        //                foreach (UnitActivation activationModelRow in activationModel)
        //                {
        //                    var unitTableRow = dbEntity.Units.Where(x => x.UnitId.Equals(activationModelRow.UnitId)).FirstOrDefault();
        //                    if (unitTableRow != null)
        //                    {
        //                        unitTableRow.IsActive = activationModelRow.IsActive;
        //                        unitTableRow.ModificationDate = DateTime.Now;
        //                        dbEntity.Entry(unitTableRow).State = EntityState.Modified;
        //                    }
        //                    else
        //                    {
        //                        notUpdatelist.Add(activationModelRow.UnitId);
        //                    }
        //                }
        //                updatedRecord = dbEntity.SaveChanges();
        //                if (updatedRecord > 0)
        //                {
        //                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Unit(s) record Changed", new { UpdatedRecord = updatedRecord, UnitIdNotExist = notUpdatelist }));
        //                }
        //                else
        //                {
        //                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Unit(s) not exist", new { UpdatedRecord = updatedRecord, UnitIdNotExist = notUpdatelist }));
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            return CatchException(ex);
        //        }
        //    }
        //    else
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Invalid UnitId/IsActive value", GetErrorList()));
        //    }
        //}

        /// <summary>
        /// To Update multiple tenant lease period
        /// </summary>
        /// <param name="model">TenantLeaseModel</param>
        /// <returns>no of updated record</returns>
        [HttpPost]
        [Route("UpdateTenantLeaseExpiry")]
        public HttpResponseMessage TenantLeaseUpdate([FromBody] List<TenantLeaseModel> model)
        {
            //// Validation Check
            if (model != null && ModelState.IsValid && model.Count > 0)
            {
                MaisonetteEntities context = null;
                try
                {
                    context = new MaisonetteEntities();
                    context.Configuration.AutoDetectChangesEnabled = false;
                    //// Hold the updated Record value
                    int updatedRecord = 0;
                    int ClientId = getHeaderClientId();
                    List<string> notify = new List<string>();
                    foreach (var item in model)
                    {
                        //// fetcting  record from db if exists
                        var rowSet = context.Units.Where(x => x.ClientId.Equals(ClientId) && x.TenantId.Equals(item.TenantId) && x.UnitId.Equals(item.UnitId)).FirstOrDefault();
                        //// if  there is no record then insert  
                        if (rowSet != null)
                        {
                            if (rowSet.TenancyEnd.Date == item.TenancyEnd.Date)
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Tenancy End Date is already exist", new { RecordUpdated = updatedRecord + " out of " + model.Count }));
                            }
                            if (item.TenancyEnd.Date < item.TenancyStart.Date)
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Tenancy End date should be greater or equal to Tenancy Start date", new { RecordUpdated = updatedRecord + " out of " + model.Count }));
                            }
                            rowSet.ModificationDate = DateTime.Now;
                                rowSet.TenancyEnd = item.TenancyEnd;
                                rowSet.TenancyStart = item.TenancyStart;
                                context.Entry(rowSet).State = EntityState.Modified;
                                #region Send Push Notification
                                var login = context.Logins.Where(x => x.TenantId.Equals(item.TenantId) && x.ClientId.Equals(ClientId)).FirstOrDefault();
                                if (login != null)
                                {
                                    if (!string.IsNullOrEmpty(login.DeviceRegToken))
                                    {
                                        NotificationData data = new NotificationData();
                                        data.NotificationType = NotificationType.LeaseUpdate.ToString();
                                        data.TicketId = 0;
                                        data.TenancyStartDate = item.TenancyEnd;
                                        data.TenancyEndDate = item.TenancyStart;
                                        data.ClientId = login.ClientId;
                                        string title = "Your Tenancy Lease Period has been changed from " + item.TenancyStart.ToShortDateString() + " to " + item.TenancyEnd.ToShortDateString();
                                        AndroidNotify androidModel = new AndroidNotify();
                                        androidModel.registration_ids = login.DeviceRegToken;
                                        androidModel.data = data;
                                        androidModel.priority = "High";
                                        androidModel.Title = title;


                                        iOSNotify iOsModel = new iOSNotify();
                                        iOsModel.notification = "Ios";
                                        iOsModel.priority = "High";
                                        iOsModel.registration_ids = login.DeviceRegToken;
                                        iOsModel.data = data;
                                        iOsModel.Title = title;

                                        if (login.DeviceType.ToLower() == "android")
                                        {
                                            string str = Utility.sendNotification(login.DeviceRegToken, androidModel, "Tenancy Lease Update", "Your Tenancy Lease Period has been changed");
                                            notify.Add(str);
                                        }
                                        else
                                        {
                                            string str = Utility.sendNotification(login.DeviceRegToken, iOsModel, "Tenancy Lease Update", "Your Tenancy Lease Period has been changed");
                                            notify.Add(str);
                                        }
                                    }
                                }
                                #endregion                          
                        }
                    }
                    //// commit data into database
                    updatedRecord = context.SaveChanges();
                    if(updatedRecord>0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Record Modified", new { RecordUpdated = updatedRecord + " out of " + model.Count }));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "No Record found", new { RecordUpdated = updatedRecord + " out of " + model.Count }));
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
                finally
                {
                    if (context != null)
                    {
                        context.Dispose();
                    }
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
            }
        }

        ///// <summary>
        ///// To Activate and Deactivate Tenant details
        ///// </summary>
        ///// <param name="activationModel">ActivationModel</param>
        ///// <returns>Activation status</returns>
        //[HttpPost]
        //[Route("TenantActivation")]
        //public HttpResponseMessage TenantActivation([FromBody] List<TenantActivation> activationModel)
        //{
        //    if (activationModel != null && ModelState.IsValid && activationModel.Count > 0)
        //    {
        //        try
        //        {
        //            List<int> notUpdatelist = new List<int>();
        //            int updatedRecord = 0;
        //            using (MaisonetteEntities dbEntity = new MaisonetteEntities())
        //            {

        //                foreach (TenantActivation activationModelRow in activationModel)
        //                {
        //                    var unitTableRow = dbEntity.Tenants.Where(x => x.TenantId.Equals(activationModelRow.TenantId)).FirstOrDefault();
        //                    if (unitTableRow != null)
        //                    {
        //                        unitTableRow.IsActive = activationModelRow.IsActive;
        //                        unitTableRow.ModificationDate = DateTime.Now;
        //                        dbEntity.Entry(unitTableRow).State = EntityState.Modified;
        //                    }
        //                    else
        //                    {
        //                        notUpdatelist.Add(activationModelRow.TenantId);
        //                    }
        //                }
        //                updatedRecord = dbEntity.SaveChanges();
        //                if (updatedRecord > 0)
        //                {
        //                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Tenant(s) record Changed", new { UpdatedRecord = updatedRecord, TenantIdNotExist = notUpdatelist }));
        //                }
        //                else
        //                {
        //                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Tenant not exist", new { UpdatedRecord = updatedRecord, TenantIdNotExist = notUpdatelist }));
        //                }
        //            }
        //        }
        //        catch (Exception ex)
        //        {
        //            return CatchException(ex);
        //        }
        //    }
        //    else
        //    {
        //        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Invalid TenantId/IsActive value", GetErrorList()));
        //    }
        //}

        /// <summary>
        /// To insert and update Login details
        /// </summary>
        /// <param name="model">LoginModel</param>
        /// <returns>no of inserted and updated record</returns>
        [HttpPost]
        [Route("TenantDetails")]
        public HttpResponseMessage TenantDetails([FromBody] List<TenantModel> model)
        {
            MaisonetteEntities context = null;
            try
            {
                //// Validation Check
                if (model != null && ModelState.IsValid && model.Count > 0)
                {
                    //// To check No. of Distinct Record in the collection
                    int distinctCount = model.GroupBy(p => p.TenantId).Select(g => g.First()).ToList().Count();
                    //// Ensure that there is no redunded record exist in the collection
                    if (distinctCount == model.Count)
                    {
                        context = new MaisonetteEntities();
                        context.Configuration.AutoDetectChangesEnabled = false;
                        //// Hold the updated Record value
                        int count = 0;
                        foreach (var item in model)
                        {
                            //// fetcting  record from db if exists
                            var rowSet = context.Tenants.Where(x => x.TenantId.Equals(item.TenantId)).FirstOrDefault();
                            //// if  there is no record then insert  
                            if (rowSet == null)
                            {
                                Tenant tenant = new Tenant();
                                tenant.TenantId = item.TenantId;
                                tenant.ClientId = item.ClientId;
                                tenant.TenantName = item.TenantName;
                                tenant.TenantNumber = item.TenantNumber;
                                tenant.Age = item.Age;
                                tenant.Sex = item.Sex.ToUpper();
                                tenant.CreationDate = DateTime.Now;
                                tenant.ModificationDate = DateTime.Now;
                                tenant.IsActive = true;
                                tenant.Address = item.Address;
                                tenant.Nationality = item.Nationality;
                                tenant.City = item.City;
                                ++count;
                                context = InsertBulk.AddToContext<Tenant>(context, tenant, count, 100, true);
                            }
                            else
                            {
                                //// if  there is any record then Update 
                                rowSet.ClientId = item.ClientId;
                                rowSet.TenantName = item.TenantName;
                                rowSet.TenantNumber = item.TenantNumber;
                                rowSet.Age = item.Age;
                                rowSet.Sex = item.Sex.ToUpper();
                                rowSet.ModificationDate = DateTime.Now;
                                rowSet.IsActive = true;
                                rowSet.Address = item.Address;
                                rowSet.Nationality = item.Nationality;
                                rowSet.City = item.City;
                                context.Entry(rowSet).State = EntityState.Modified;
                            }
                        }
                        //// commit data into database
                        context.SaveChanges();
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Record Change", new { RecordInserted = count, RecordUpdated = distinctCount - count }));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Some duplicate Tenant Id exists in supplied Collection", null));
                    }

                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
                }
            }
            catch (Exception ex)
            {
                return CatchException(ex);
            }
            finally
            {
                if (context != null)
                {
                    context.Dispose();
                }
            }
        }

        /// <summary>
        /// To insert and update Login details
        /// </summary>
        /// <param name="model">LoginModel</param>
        /// <returns>no of inserted and updated record</returns>
        [HttpPost]
        [Route("LoginDetails")]
        public HttpResponseMessage LoginDetails([FromBody] List<LoginModel> loginInput)
        {
            //// Validation Check
            if (loginInput != null && ModelState.IsValid && loginInput.Count > 0)
            {
                MaisonetteEntities context = null;
                try
                {
                    //// To check No. of Distinct Record in the collection
                    int distinctCount = loginInput.GroupBy(p => new { p.TenantId, p.ClientId }).Select(g => g.First()).ToList().Count();
                    //// Ensure that there is no redunded record exist in the collection
                    if (distinctCount == loginInput.Count)
                    {
                        context = new MaisonetteEntities();
                        context.Configuration.AutoDetectChangesEnabled = false;
                        //// Hold the updated Record value
                        int count = 0;
                        foreach (var item in loginInput)
                        {
                            //// fetcting  record from db if exists
                            var rowSet = context.Logins.Where(x => x.ClientId.Equals(item.ClientId) && x.TenantId.Equals(item.TenantId)).FirstOrDefault();
                            //// if  there is no record then insert 
                            if (rowSet == null)
                            {
                                Login login = new Login();
                                login.Mobile = item.Mobile;
                                login.ClientId = item.ClientId;
                                login.TenantId = item.TenantId;
                                login.CreationDate = DateTime.Now;
                                login.ModificationDate = DateTime.Now;
                                login.IsActive = true;
                                login.ISDCode = item.ISDCode;
                                login.LastLogin = DateTime.Now;
                                ++count;
                                context = InsertBulk.AddToContext<Login>(context, login, count, 100, true);
                            }
                            else
                            {
                                //// if  there is any record then Update 
                                rowSet.Mobile = item.Mobile;
                                rowSet.ClientId = item.ClientId;
                                rowSet.TenantId = item.TenantId;
                                rowSet.CreationDate = DateTime.Now;
                                rowSet.ModificationDate = DateTime.Now;
                                rowSet.IsActive = true;
                                rowSet.ISDCode = item.ISDCode;
                                context.Entry(rowSet).State = EntityState.Modified;
                            }
                        }
                        context.SaveChanges(); //// commit data into database
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordInserted, "Record Changed", new { RecordInserted = count, RecordUpdated = distinctCount - count }));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Some duplicate set {Client Id,Tenant Id} exists in supplied Collection", null));
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
                finally
                {
                    if (context != null)
                    {
                        context.Dispose();
                    }
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
            }
        }

        /// <summary>
        /// To Activate and Deactivate Login details
        /// </summary>
        /// <param name="activationModel">ActivationModel</param>
        /// <returns>Activation status</returns>
        [HttpPost]
        [Route("LoginActivation")]
        public HttpResponseMessage LoginActivation([FromBody] List<LoginActivation> activationModel)
        {
            if (activationModel != null && ModelState.IsValid && activationModel.Count > 0)
            {
                try
                {
                    List<int> notUpdatelist = new List<int>();
                    int updatedRecord = 0;
                    using (MaisonetteEntities dbEntity = new MaisonetteEntities())
                    {

                        foreach (LoginActivation activationModelRow in activationModel)
                        {
                            var loginTableRow = dbEntity.Logins.Where(x => x.TenantId.Equals(activationModelRow.TenantId)).FirstOrDefault();
                            if (loginTableRow != null)
                            {
                                loginTableRow.IsActive = activationModelRow.IsActive;
                                loginTableRow.ModificationDate = DateTime.Now;
                                dbEntity.Entry(loginTableRow).State = EntityState.Modified;
                            }
                            else
                            {
                                notUpdatelist.Add(activationModelRow.TenantId);
                            }
                        }
                        updatedRecord = dbEntity.SaveChanges();
                        if (updatedRecord > 0)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Tenant(s) record Changed", new { UpdatedRecord = updatedRecord, TenantIdNotExist = notUpdatelist }));
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Tenant(s) not Exist", new { UpdatedRecord = updatedRecord, TenantIdNotExist = notUpdatelist }));
                        }
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Invalid TenantId/IsActive value", GetErrorList()));
            }
        }

        /// <summary>
        /// To insert and update Client details
        /// </summary>
        /// <param name="model">ClientModel</param>
        /// <returns>no of inserted and updated record</returns>
        [HttpPost]
        [Route("ClientDetails")]
        public HttpResponseMessage ClientDetails([FromBody] List<ClientModel> clientInput)
        {
            if (clientInput != null && ModelState.IsValid && clientInput.Count > 0) //// Validation Check
            {
                MaisonetteEntities context = null;
                try
                {
                    int distinctCount = clientInput.GroupBy(p => p.ClientId).Select(g => g.First()).ToList().Count(); //// To check No. of Distinct Record in the collection
                    if (distinctCount == clientInput.Count) //// Ensure that there is no redunded record exist in the collection
                    {
                        context = new MaisonetteEntities();
                        context.Configuration.AutoDetectChangesEnabled = false;
                        int count = 0; //// Hold the updated Record value
                        foreach (var clientCollection in clientInput)
                        {
                            var rowSet = context.Clients.Where(x => x.ClientID.Equals(clientCollection.ClientId)).FirstOrDefault(); //// fetcting  record from db if exists
                            if (rowSet == null) //// if  there is no record then insert 
                            {
                                Client client = new Client();
                                client.Address = clientCollection.Address;
                                client.City = clientCollection.City;
                                client.ClientID = clientCollection.ClientId;
                                client.ClientNumber = clientCollection.ClientNumber;
                                client.CreationDate = DateTime.Now;
                                client.ModificationDate = DateTime.Now;
                                client.IsActive = true;
                                client.Name = clientCollection.Name;
                                client.Nationality = clientCollection.Nationality;
                                client.PersonName = clientCollection.PersonName;
                                client.Email = clientCollection.Email;
                                client.InternationalCode_Prefix = clientCollection.InternationalCode;
                                client.ContactNumber = clientCollection.ContactNumber;
                                ++count;
                                context = InsertBulk.AddToContext<Client>(context, client, count, 100, true);
                            }
                            else
                            {
                                //// if  there is any record then Update 
                                rowSet.Address = clientCollection.Address;
                                rowSet.City = clientCollection.City;
                                rowSet.ClientID = clientCollection.ClientId;
                                rowSet.ClientNumber = clientCollection.ClientNumber;
                                rowSet.ModificationDate = DateTime.Now;
                                rowSet.IsActive = true;
                                rowSet.Name = clientCollection.Name;
                                rowSet.Nationality = clientCollection.Nationality;
                                rowSet.PersonName = clientCollection.PersonName;
                                rowSet.Email = clientCollection.Email;
                                rowSet.InternationalCode_Prefix = clientCollection.InternationalCode;
                                rowSet.ContactNumber = clientCollection.ContactNumber;
                                context.Entry(rowSet).State = EntityState.Modified;
                            }
                        }
                        context.SaveChanges(); //// commit data into database
                        // return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordInserted, "Record Modified", new { RecordInserted = count, RecordUpdated = distinctCount - count }));
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Successful, "Record Inserted/Modified", new { RecordInserted = count, RecordUpdated = distinctCount - count }));

                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Some duplicate Client Id exists in supplied Collection", null));
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
                finally
                {
                    if (context != null)
                    {
                        context.Dispose();
                    }
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
            }
        }

        /// <summary>
        /// To Activate and Deactivate Client details
        /// </summary>
        /// <param name="activationModel">ActivationModel</param>
        /// <returns>Activation status</returns>
        [HttpPost]
        [Route("ClientActivation")]
        public HttpResponseMessage ClientActivation([FromBody] List<ClientActivation> activationModel)
        {
            if (activationModel != null && ModelState.IsValid && activationModel.Count > 0)
            {
                try
                {
                    List<int> notUpdatelist = new List<int>();
                    int updatedRecord = 0;
                    using (MaisonetteEntities dbEntity = new MaisonetteEntities())
                    {

                        foreach (ClientActivation activationModelRow in activationModel)
                        {
                            var unitTableRow = dbEntity.Clients.Where(x => x.ClientID.Equals(activationModelRow.ClientId)).FirstOrDefault();
                            if (unitTableRow != null)
                            {
                                unitTableRow.IsActive = activationModelRow.IsActive;
                                unitTableRow.ModificationDate = DateTime.Now;
                                dbEntity.Entry(unitTableRow).State = EntityState.Modified;
                            }
                            else
                            {
                                notUpdatelist.Add(activationModelRow.ClientId);
                            }
                        }
                        updatedRecord = dbEntity.SaveChanges();
                        if (updatedRecord > 0)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Client(s) record Changed", new { UpdatedRecord = updatedRecord, TenantIdNotExist = notUpdatelist }));
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Client(s) not exist", new { UpdatedRecord = updatedRecord, TenantIdNotExist = notUpdatelist }));
                        }
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Invalid ClientId/IsActive value", GetErrorList()));
            }
        }

        /// <summary>
        /// To insert and update Component details
        /// </summary>
        /// <param name="model">ComponentModel class</param>
        /// <returns>no of inserted and updated record</returns>
        [HttpPost]
        [Route("ComponentCollection")]
        public HttpResponseMessage ComponentCollection([FromBody] List<ComponentModel> model)
        {
            if (model != null && ModelState.IsValid && model.Count > 0) //// Validation Check
            {
                MaisonetteEntities context = null;
                try
                {
                    int distinctCount = model.GroupBy(p => p.ComponentId).Select(g => g.First()).ToList().Count(); //// To check No. of Distinct Record in the collection
                    if (distinctCount == model.Count) //// Ensure that there is no redunded record exist in the collection
                    {
                        context = new MaisonetteEntities();
                        context.Configuration.AutoDetectChangesEnabled = false;
                        int count = 0;//// Hold the updated Record value
                        foreach (var item in model)
                        {
                            var rowSet = context.Components.Where(x => x.ComponentId.Equals(item.ComponentId)).FirstOrDefault(); //// fetcting  record from db if exists
                            if (rowSet == null) //// if  there is no record then insert 
                            {
                                Component component = new Component();
                                component.Address = item.Address;
                                component.City = item.City;
                                component.ClientID = item.ClientId;
                                component.ComponentId = item.ComponentId;
                                component.ComponentNumber = item.ComponentNumber;
                                component.CreationDate = DateTime.Now;
                                component.ModificationDate = DateTime.Now;
                                component.IsActive = true;
                                component.Nationality = item.Nationality;
                                component.PlotNo = item.PlotNo;
                                component.Sector = item.Sector;
                                component.Region = item.Region;
                                component.PoBox = item.PoBox;
                                ++count;
                                context = InsertBulk.AddToContext<Component>(context, component, count, 100, true);
                                
                            }
                            else
                            {
                                //// if  there is any record then Update 
                                rowSet.Address = item.Address;
                                rowSet.City = item.City;
                                rowSet.ClientID = item.ClientId;
                                rowSet.ComponentNumber = item.ComponentNumber;
                                rowSet.ModificationDate = DateTime.Now;
                                rowSet.IsActive = true;
                                rowSet.Nationality = item.Nationality;
                                rowSet.PlotNo = item.PlotNo;
                                rowSet.Sector = item.Sector;
                                rowSet.Region = item.Region;
                                rowSet.PoBox = item.PoBox;
                                context.Entry(rowSet).State = EntityState.Modified;
                               
                            }
                        }
                        context.SaveChanges(); //// commit data into database
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordInserted, "Record Modified", new { RecordInserted = count, RecordUpdated = distinctCount - count }));
                        
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Some duplicate Component Id exists in supplied Collection", null));
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
                finally
                {
                    if (context != null)
                    {
                        context.Dispose();
                    }
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
            }
        }

        /// <summary>
        /// To Activate and Deactivate Component details
        /// </summary>
        /// <param name="activationModel">ActivationModel</param>
        /// <returns>Activation status</returns>
        [HttpPost]
        [Route("ComponentActivation")]
        public HttpResponseMessage ComponentActivation([FromBody] List<ComponentActivation> activationModel)
        {
            if (activationModel != null && ModelState.IsValid && activationModel.Count > 0)
            {
                try
                {
                    List<int> notUpdatelist = new List<int>();
                    int updatedRecord = 0;
                    using (MaisonetteEntities dbEntity = new MaisonetteEntities())
                    {

                        foreach (ComponentActivation activationModelRow in activationModel)
                        {
                            var componentTableRow = dbEntity.Components.Where(x => x.ComponentId.Equals(activationModelRow.ComponentId)).FirstOrDefault();
                            if (componentTableRow != null)
                            {
                                componentTableRow.IsActive = activationModelRow.IsActive;
                                componentTableRow.ModificationDate = DateTime.Now;
                                dbEntity.Entry(componentTableRow).State = EntityState.Modified;
                            }
                            else
                            {
                                notUpdatelist.Add(activationModelRow.ComponentId);
                            }
                        }
                        updatedRecord = dbEntity.SaveChanges();
                        if (updatedRecord > 0)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Component(s) record Changed", new { UpdatedRecord = updatedRecord, ComponentIdNotExist = notUpdatelist }));
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Component(s) not found", new { UpdatedRecord = updatedRecord, ComponentIdNotExist = notUpdatelist }));
                        }
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Invalid ComponentId/IsActive value", GetErrorList()));
            }
        }

        /// <summary>
        /// Batch update of Tenant and respective master
        /// </summary>
        /// <param name="tenantBatchInput">TenantBatchModel</param>
        /// <returns>no. of inserted record</returns>
        [HttpPost]
        [Route("TenantBatchEntry")]
        public HttpResponseMessage TenantBatchEntry([FromBody] TenantBatchModel tenantBatchInput)
        {
            //// Validation Check
            if (tenantBatchInput != null && ModelState.IsValid)
            {
                int count = 0,ModifiedRecord=0;
                MaisonetteEntities context = null;
                context = new MaisonetteEntities();
                context.Configuration.AutoDetectChangesEnabled = false;
                using (var transaction = context.Database.BeginTransaction())
                {
                    try
                    {

                        if (tenantBatchInput.ClientCollection != null)
                        {
                            count = 0;
                            foreach (var currentClientIpnut in tenantBatchInput.ClientCollection)
                            {
                                //// fetcting  record from db if exists
                                var clientTableRow = context.Clients.Where(column => column.ClientID.Equals(currentClientIpnut.ClientId)).FirstOrDefault();
                                //// if  there is no record then insert  
                                if (clientTableRow == null)
                                {
                                    Client newClient = new Client();
                                    newClient.Address = currentClientIpnut.Address;
                                    newClient.City = currentClientIpnut.City;
                                    newClient.ClientID = currentClientIpnut.ClientId;
                                    newClient.ClientNumber = currentClientIpnut.ClientNumber;
                                    newClient.CreationDate = DateTime.Now;
                                    newClient.ModificationDate = DateTime.Now;
                                    newClient.IsActive = true;
                                    newClient.Name = currentClientIpnut.Name;
                                    newClient.Nationality = currentClientIpnut.Nationality;
                                    newClient.PersonName = currentClientIpnut.PersonName;
                                    newClient.InternationalCode_Prefix = currentClientIpnut.InternationalCode;
                                    newClient.ContactNumber = currentClientIpnut.ContactNumber;
                                    newClient.Email = currentClientIpnut.Email;
                                    ++count;
                                    context = InsertBulk.AddToContext<Client>(context, newClient, count, 100, true);
                                }
                                else
                                {
                                    clientTableRow.Address = currentClientIpnut.Address;
                                    clientTableRow.City = currentClientIpnut.City;
                                    clientTableRow.ClientNumber = currentClientIpnut.ClientNumber;
                                    clientTableRow.ModificationDate = DateTime.Now;
                                    clientTableRow.IsActive = true;
                                    clientTableRow.Name = currentClientIpnut.Name;
                                    clientTableRow.Nationality = currentClientIpnut.Nationality;
                                    clientTableRow.PersonName = currentClientIpnut.PersonName;
                                    clientTableRow.InternationalCode_Prefix = currentClientIpnut.InternationalCode;
                                    clientTableRow.ContactNumber = currentClientIpnut.ContactNumber;
                                    clientTableRow.Email = currentClientIpnut.Email;
                                    context.Entry(clientTableRow).State = EntityState.Modified;
                                    ModifiedRecord += context.SaveChanges();
                                }
                            }
                        }
                        if (tenantBatchInput.ComponentCollection != null)
                        {
                            count = 0;
                            foreach (var currentComponentIpnut in tenantBatchInput.ComponentCollection)
                            {
                                //// fetcting  record from db if exists
                                var componentTableRow = context.Components.Where(column => column.ComponentId.Equals(currentComponentIpnut.ComponentId)).FirstOrDefault();
                                //// if  there is no record then insert  
                                if (componentTableRow == null)
                                {
                                    Component newComponent = new Component();
                                    newComponent.Address = currentComponentIpnut.Address;
                                    newComponent.City = currentComponentIpnut.City;
                                    newComponent.ClientID = currentComponentIpnut.ClientId;
                                    newComponent.ComponentId = currentComponentIpnut.ComponentId;
                                    newComponent.ComponentNumber = currentComponentIpnut.ComponentNumber;
                                    newComponent.ComponentName = currentComponentIpnut.ComponentName;
                                    newComponent.CreationDate = DateTime.Now;
                                    newComponent.ModificationDate = DateTime.Now;
                                    newComponent.IsActive = true;
                                    newComponent.Nationality = currentComponentIpnut.Nationality;
                                    newComponent.PlotNo = currentComponentIpnut.PlotNo;
                                    newComponent.Sector = currentComponentIpnut.Sector;
                                    newComponent.Region = currentComponentIpnut.Region;
                                    newComponent.PoBox = currentComponentIpnut.PoBox;
                                    ++count;
                                    context = InsertBulk.AddToContext<Component>(context, newComponent, count, 100, true);
                                }
                                else
                                {
                                    componentTableRow.Address = currentComponentIpnut.Address;
                                    componentTableRow.City = currentComponentIpnut.City;
                                    componentTableRow.ClientID = currentComponentIpnut.ClientId;
                                    componentTableRow.ComponentNumber = currentComponentIpnut.ComponentNumber;
                                    componentTableRow.ComponentName = currentComponentIpnut.ComponentName;
                                    componentTableRow.ModificationDate = DateTime.Now;
                                    componentTableRow.IsActive = true;
                                    componentTableRow.Nationality = currentComponentIpnut.Nationality;
                                    componentTableRow.PlotNo = currentComponentIpnut.PlotNo;
                                    componentTableRow.Sector = currentComponentIpnut.Sector;
                                    componentTableRow.Region = currentComponentIpnut.Region;
                                    componentTableRow.PoBox = currentComponentIpnut.PoBox;
                                    context.Entry(componentTableRow).State = EntityState.Modified;
                                    ModifiedRecord += context.SaveChanges();
                                }
                            }
                        }
                        if (tenantBatchInput.UnitCollection != null)
                        {
                            count = 0;
                            foreach (var currentUnitIpnut in tenantBatchInput.UnitCollection)
                            {
                                //// fetcting  record from db if exists
                                var unitTableRow = context.Units.Where(column => column.UnitId.Equals(currentUnitIpnut.UnitId)).FirstOrDefault();
                                //// if  there is no record then insert  
                                if (unitTableRow == null)
                                {
                                    Unit newUnit = new Unit();
                                    newUnit.ComponentId = currentUnitIpnut.ComponentId;
                                    newUnit.TenancyEnd = currentUnitIpnut.TenancyEnd;
                                    newUnit.TenancyStart = currentUnitIpnut.TenancyStart;
                                    newUnit.TenantId = currentUnitIpnut.TenantId;
                                    newUnit.CreationDate = DateTime.Now;
                                    newUnit.ModificationDate = DateTime.Now;
                                    newUnit.IsActive = true;
                                    newUnit.ComponentNumber = currentUnitIpnut.ComponentNumber;
                                    newUnit.UnitId = currentUnitIpnut.UnitId;
                                    newUnit.ClientId = currentUnitIpnut.ClientId;
                                    newUnit.UnitNumber = currentUnitIpnut.UnitNumber;
                                    ++count;
                                    context = InsertBulk.AddToContext<Unit>(context, newUnit, count, 100, true);
                                }
                                else
                                {
                                    unitTableRow.ComponentId = currentUnitIpnut.ComponentId;
                                    unitTableRow.TenancyEnd = currentUnitIpnut.TenancyEnd;
                                    unitTableRow.TenancyStart = currentUnitIpnut.TenancyStart;
                                    unitTableRow.TenantId = currentUnitIpnut.TenantId;
                                    unitTableRow.ModificationDate = DateTime.Now;
                                    unitTableRow.IsActive = true;
                                    unitTableRow.ComponentNumber = currentUnitIpnut.ComponentNumber;
                                    unitTableRow.ClientId = currentUnitIpnut.ClientId;
                                    unitTableRow.UnitNumber = currentUnitIpnut.UnitNumber;
                                    context.Entry(unitTableRow).State = EntityState.Modified;
                                    ModifiedRecord += context.SaveChanges();
                                }
                            }
                        }
                        if (tenantBatchInput.TenantCollection != null)
                        {
                            count = 0;
                            foreach (var currentTenantIpnut in tenantBatchInput.TenantCollection)
                            {
                                //// fetcting  record from db if exists
                                var tenantTableRow = context.Tenants.Where(column => column.TenantId.Equals(currentTenantIpnut.TenantId)).FirstOrDefault();
                                //// if  there is no record then insert  
                                if (tenantTableRow == null)
                                {
                                    Tenant newTenant = new Tenant();
                                    newTenant.ClientId = currentTenantIpnut.ClientId;
                                    newTenant.TenantId = currentTenantIpnut.TenantId;
                                    newTenant.TenantName = currentTenantIpnut.TenantName;
                                    newTenant.TenantNumber = currentTenantIpnut.TenantNumber;
                                    newTenant.Age = currentTenantIpnut.Age;
                                    newTenant.Sex = currentTenantIpnut.Sex;
                                    newTenant.Address = currentTenantIpnut.Address;
                                    newTenant.Nationality = currentTenantIpnut.Nationality;
                                    newTenant.City = currentTenantIpnut.City;
                                    newTenant.IsActive = true;
                                    newTenant.ModificationDate = DateTime.Now;
                                    newTenant.CreationDate = DateTime.Now;
                                    ++count;
                                    context = InsertBulk.AddToContext<Tenant>(context, newTenant, count, 100, true);
                                }
                                else
                                {
                                    tenantTableRow.ClientId = currentTenantIpnut.ClientId;
                                    tenantTableRow.TenantName = currentTenantIpnut.TenantName;
                                    tenantTableRow.TenantNumber = currentTenantIpnut.TenantNumber;
                                    tenantTableRow.Age = currentTenantIpnut.Age;
                                    tenantTableRow.Sex = currentTenantIpnut.Sex;
                                    tenantTableRow.Address = currentTenantIpnut.Address;
                                    tenantTableRow.Nationality = currentTenantIpnut.Nationality;
                                    tenantTableRow.City = currentTenantIpnut.City;
                                    tenantTableRow.IsActive = true;
                                    tenantTableRow.ModificationDate = DateTime.Now;
                                    context.Entry(tenantTableRow).State = EntityState.Modified;
                                    ModifiedRecord += context.SaveChanges();
                                }
                            }
                        }
                        if (tenantBatchInput.LoginCollection != null)
                        {
                            foreach (var currentLoginIpnut in tenantBatchInput.LoginCollection)
                            {
                                //// fetcting  record from db if exists
                                var loginTableRow = context.Logins.Where(column => column.TenantId.Equals(currentLoginIpnut.TenantId)).FirstOrDefault();
                                //// if  there is no record then insert  
                                if (loginTableRow == null)
                                {
                                    Login newLogin = new Login();
                                    newLogin.ClientId = currentLoginIpnut.ClientId;
                                    newLogin.TenantId = currentLoginIpnut.TenantId;
                                    newLogin.LastLogin = DateTime.Now;
                                    newLogin.Mobile = currentLoginIpnut.Mobile;
                                    newLogin.IsActive = true;
                                    newLogin.IsVerified = false;
                                    newLogin.ModificationDate = DateTime.Now;
                                    newLogin.CreationDate = DateTime.Now;
                                    newLogin.ISDCode = currentLoginIpnut.ISDCode;
                                    ++count;
                                    context = InsertBulk.AddToContext<Login>(context, newLogin, count, 100, true);
                                }
                                else
                                {
                                    loginTableRow.ClientId = currentLoginIpnut.ClientId;
                                    loginTableRow.LastLogin = DateTime.Now;
                                    loginTableRow.Mobile = currentLoginIpnut.Mobile;
                                    loginTableRow.IsActive = true;
                                    loginTableRow.IsVerified = false;
                                    loginTableRow.ModificationDate = DateTime.Now;
                                    loginTableRow.ISDCode = currentLoginIpnut.ISDCode;
                                    context.Entry(loginTableRow).State = EntityState.Modified;
                                    ModifiedRecord += context.SaveChanges();

                                }
                            }
                        }

                        //// commit data into database
                        context.SaveChanges();
                        transaction.Commit();
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Record Inserted/Modified", new { RecordInserted = count,RecordModified=ModifiedRecord }));
                    }
                    catch (Exception ex)
                    {
                        transaction.Rollback();
                        return CatchException(ex);                       
                    }
                    finally
                    {
                        if (context != null)
                        {
                            context.Dispose();
                        }
                    }
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
            }
        }

        /// <summary>
        /// To cancel the ticket
        /// </summary>
        /// <param name="TicketId">Ticket unique id</param>
        /// <param name="CancelNote">Desc for ticket cancellation</param>
        /// <returns></returns>
        [HttpPost]
        [Route("CancelTicket")]
        public HttpResponseMessage CancelTicket([FromBody] TicketCancelModel InputModel)
        {
            if (InputModel != null && ModelState.IsValid)
            {
                try
                {
                    using (MaisonetteEntities entity = new MaisonetteEntities())
                    {
                        var existTicket = entity.Tickets.Where(column => column.TicketNo == InputModel.TicketNo).FirstOrDefault();
                        if (existTicket != null)
                        {
                            var cancelStatusId = entity.TicketStatusMasters.Where(column => column.StatusType.ToLower().Contains("cancel") && column.IsActive).FirstOrDefault();
                            if (cancelStatusId != null)
                            {
                                existTicket.CancelNote = InputModel.CancelNote;
                                existTicket.ModificationDate = DateTime.Now;
                                existTicket.StatusId = cancelStatusId.Id;
                               // existTicket.IsSync = false;
                                entity.Entry(existTicket).State = EntityState.Modified;
                                entity.SaveChanges();
                                return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Ticket has been cancelled", null));
                            }
                            else
                                return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Cancel status is not mapped in Master data", "make an entry in TicketStatusMaster table for cancel status"));
                        }
                        else
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "No ticket found for Ticket No : " + InputModel.TicketNo, null));
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid TicketId/CancelNote supplied", GetErrorList()));

        }

        /// <summary>
        /// it will retrive and return all validation error from Model State
        /// </summary>
        /// <returns>List of validation error</returns>
        private IEnumerable<ModelError> GetErrorList()
        {
            return ModelState.Values.SelectMany(v => v.Errors);
        }

        private HttpResponseMessage CatchException(Exception ex)
        {
            try
            {
                SqlException sqlException = ex.InnerException.InnerException as SqlException;
                if (sqlException != null)
                {
                    switch (sqlException.Number)
                    {
                        case 2601:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.UniqueKeyViolation, "Cant insert Duplicate Key value", sqlException.Message));
                        case 2627:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.PrimaryKeyViolation, "Cant insert Duplicate Key value", sqlException.Message));
                        case 547:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ForeignKeyViolation, "Cant insert which is not exist in Master Data", sqlException.Message));
                        case 515:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.NullValueViolation, "Cant insert Null value", sqlException.Message));
                        default:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.InvalidValue, "Invalid Value Supplied", sqlException.Message));
                    }
                }
                else
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, GenericStatus.GetGenericStatus(GenericStatusCode.InvalidValue, "Some Invalid Value Supplied :" + ex.Message));
            }
            catch (Exception exx)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, GenericStatus.GetGenericStatus(GenericStatusCode.InvalidValue, "Some Invalid Value Supplied :" + exx.Message));
            }
        }

        private int getHeaderClientId()
        {
            try
            {
                IEnumerable<string> headerValues;
                var ClientId = 0;
                if (Request.Headers.TryGetValues("ClientId", out headerValues))
                {
                    ClientId = Convert.ToInt32(headerValues.FirstOrDefault());
                }

                return ClientId;
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
