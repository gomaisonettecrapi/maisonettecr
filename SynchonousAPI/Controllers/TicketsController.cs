﻿using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using DataModel;
using Newtonsoft.Json;
using SynchonousAPI.Models;
using SynchronousAPI.Models;
using System.Web.Http.ModelBinding;
using System;
using System.Data.SqlClient;
using SynchonousAPI.Filter;
using System.Data.Entity;

namespace SynchonousAPI.Controllers
{
    [ValidateAPIKey]
    [RoutePrefix("api/Ticket")]
    public class TicketsController : ApiController
    {
        [HttpGet]
        [Route("GetTickets")]
        public HttpResponseMessage TicketList()
        {
            try
            {
                    var ClientId = getHeaderClientId();
                    MaisonetteEntities entity = new MaisonetteEntities();

                    var result = (
                                    from tics in entity.Tickets
                                    join req in entity.RequestTypeMasters on tics.RequestTypeId equals req.RequestTypeId
                                    join status in entity.TicketStatusMasters on tics.StatusId equals status.Id
                                    where tics.ClientId.Equals(ClientId) && tics.IsSync==false && req.IsActive && status.IsActive && tics.IsActive == true && tics.ClientId.Equals(req.ClientId)
                                    orderby tics.Id descending
                                    select new { tics.TenantId, tics.TicketNo, TicketId = tics.Id, tics.RequestTitle, tics.RequestDecription, tics.Priority, tics.ClientId, status.StatusType, tics.IsSync, req.RequestTypeId, req.RequestType, tics.CreationDate, tics.LocationType, tics.LocationId }

                                 );

                    if (result != null && result.ToList().Count > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataFound, "Record found", result));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "No ticket found for Client Id : " + ClientId, null));
                    }
            }
            catch (Exception ex)
            {
                return CatchException(ex);
            }
        }

        [HttpGet]
        [Route("GetCancelledTickets")]
        public HttpResponseMessage CancelTicketList()
        {
            try
            {
                var ClientId = getHeaderClientId();
                MaisonetteEntities entity = new MaisonetteEntities();

                var cancelTicketList = (
                                from tics in entity.Tickets
                                where tics.IsActive == true && tics.ClientId.Equals(ClientId) && tics.IsCancelSync == false && tics.StatusId == (int)TicketStatus.Cancelled
                                orderby tics.Id descending
                                select new { tics.TenantId, tics.TicketNo, tics.CancelNote, tics.CancellationDate,tics.IsCancelSync,tics.IsSync }
                             );

                if (cancelTicketList != null && cancelTicketList.ToList().Count > 0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataFound, "Record found", cancelTicketList));
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "No cancel ticket found for client Id : " + ClientId, null));
                }
            }
            catch (Exception ex)
            {
                return CatchException(ex);
            }
        }

        [HttpPost]
        [Route("CancelTicketAck")]
        public HttpResponseMessage CancelTicketAck([FromBody] List<TicketIdModel> inputModel)
        {
            //// Validation Check

            if (inputModel != null && ModelState.IsValid && inputModel.Count > 0)
            {
                var ClientId = getHeaderClientId();
                int totalTickets = inputModel.Count;
                MaisonetteEntities context = null;
                try
                {
                    context = new MaisonetteEntities();
                    context.Configuration.AutoDetectChangesEnabled = false;
                    //// Hold the updated Record value
                    int updatedRecord = 0;
                    List<string> invalidTicketNo= new List<string>();
                    List<string> AlreadySync = new List<string>();
                    foreach (var item in inputModel)
                    {
                        //// fetcting  record from db if exists
                        var rowSet = context.Tickets.Where(x => x.TicketNo.Equals(item.TicketNo) & x.IsActive == true && x.ClientId.Equals(ClientId)).FirstOrDefault();
                        //// if  there is no record then insert  
                        if (rowSet != null)
                        {
                            if (rowSet.IsCancelSync == false)
                            {
                                rowSet.IsCancelSync = true;
                                rowSet.ModificationDate = DateTime.Now;
                                context.Entry(rowSet).State = EntityState.Modified;
                            }
                            else
                            {
                                AlreadySync.Add(item.TicketNo);
                            }
                        }
                        else
                        {
                            invalidTicketNo.Add(item.TicketNo);
                        }
                    }
                    //// commit data into database
                    updatedRecord = context.SaveChanges();
                    if (updatedRecord > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordInserted, "Record Modified", new { RecordUpdated = updatedRecord + " out of " + totalTickets, invalidTicketId = invalidTicketNo, AlreadySync = AlreadySync }));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Record not found", new { RecordUpdated = updatedRecord + " out of " + totalTickets, invalidTicketId = invalidTicketNo, AlreadySync = AlreadySync }));
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
                finally
                {
                    if (context != null)
                    {
                        context.Dispose();
                    }
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
            }
        }

        [HttpGet]
        [Route("GetTicketAttachment")]
        public HttpResponseMessage GetTicketAttachment([FromUri]String TicketNo)
        {
            try
            {
                if (!String.IsNullOrEmpty(TicketNo))
                {
                    var ClientId = getHeaderClientId();
                    MaisonetteEntities entity = new MaisonetteEntities();
                    var AttchmentDetails = (
                                   from tics in entity.Tickets
                                   from Attach in entity.Attachments.Where(mapping => mapping.ClientID == tics.ClientId && mapping.TicketId == tics.Id)
                                       //where tics.IsActive == true && tics.ClientId.Equals(ClientId) && Attach.IsActive == true && tics.IsSync==false && Attach.IsSync==false && tics.TicketNo.Equals(TicketNo)
                                   where tics.IsActive == true && tics.ClientId.Equals(ClientId) && Attach.IsActive == true && Attach.IsSync == false && tics.TicketNo.Equals(TicketNo)
                                   orderby tics.Id descending
                                   select new { TicketNo = tics.TicketNo, AttachmentId = Attach.Id, Attach.IsSync, Attachment = Attach.Attachment1 }
                                );

                    if (AttchmentDetails.ToList().Count > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataFound, "Record found", new
                        {
                            AttchmentDetails
                        }));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, TicketNo + " Not found for client Id : " + ClientId, null));
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Please pass a TicketNo in URI  : ", null));
                }
            }
            catch (Exception ex)
            {
                return CatchException(ex);
            }
        }

        [HttpPost]
        [Route("TicketAttachmentAck")]
        public HttpResponseMessage TicketAttachmentAck([FromBody] List<TicketIdModel> inputModel)
        {
            try
            {
                if (ModelState.IsValid && inputModel.Count>0)
                {
                    var ClientId = getHeaderClientId();
                    MaisonetteEntities entity = new MaisonetteEntities();
                    List<string> InvalidTicketNo = new List<string>();
                    List<string> AlreadySyncTicketNo = new List<string>();
                    List<string> NoAttachment = new List<string>();
                    int Updatetd = 0;
                    foreach (TicketIdModel item in inputModel)
                    {
                        var currentTicket = entity.Tickets.Where(tic => tic.ClientId.Equals(ClientId) && tic.TicketNo.ToLower().Equals(item.TicketNo.Trim().ToLower()) && tic.IsActive == true).FirstOrDefault();
                        if(currentTicket!=null)
                        {
                            var attach = entity.Attachments.Where(att => att.TicketId.Equals(currentTicket.Id) && att.IsActive == true).FirstOrDefault();
                            if(attach==null)
                            {
                                NoAttachment.Add(item.TicketNo);
                            }
                            else if(attach.IsSync==true)
                            {
                                AlreadySyncTicketNo.Add(item.TicketNo);
                            }
                            else
                            {
                                attach.IsSync = true;
                                attach.ModificationDate = DateTime.Now;
                                entity.Entry(attach).State = EntityState.Modified;
                                Updatetd += entity.SaveChanges();
                            }
                        }
                        else
                        {
                            InvalidTicketNo.Add(item.TicketNo);
                        }
                    }
                    if (Updatetd > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Record Modified", new { UpdatedRecord = Updatetd + " out of " + inputModel.Count, InvalidTicketNo = InvalidTicketNo, AlreadySyncTicketNo = AlreadySyncTicketNo, NoAttachment = NoAttachment }));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Record Not Modified", new { UpdatedRecord = Updatetd + " out of " + inputModel.Count, InvalidTicketNo = InvalidTicketNo, AlreadySyncTicketNo = AlreadySyncTicketNo, NoAttachment = NoAttachment }));
                    }
                }
                else
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
                }
            }
            catch (Exception ex)
            {
                return CatchException(ex);
            }
        }

        private IEnumerable<ModelError> GetErrorList()
        {
            return ModelState.Values.SelectMany(v => v.Errors);
        }

        private HttpResponseMessage CatchException(Exception ex)
        {
            try
            {
                SqlException sqlException = ex.InnerException.InnerException as SqlException;
                if (sqlException != null)
                {
                    switch (sqlException.Number)
                    {
                        case 2601:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.UniqueKeyViolation, "Cant insert Duplicate Key value", sqlException.Message));
                        case 2627:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.PrimaryKeyViolation, "Cant insert Duplicate Key value", sqlException.Message));
                        case 547:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ForeignKeyViolation, "Cant insert which is not exist in Master Data", sqlException.Message));
                        case 515:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.NullValueViolation, "Cant insert Null value", sqlException.Message));
                        default:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.InvalidValue, "Cant insert Duplicate Key value", sqlException.Message));
                    }
                }
                else
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, GenericStatus.GetGenericStatus(GenericStatusCode.InvalidValue, "Some Invalid Value Supplied " + ex.Message));
            }
            catch (Exception exx)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, GenericStatus.GetGenericStatus(GenericStatusCode.InvalidValue, "Some Invalid Value Supplied " + exx.Message));
            }
        }

        private int getHeaderClientId()
        {
            try
            {
                IEnumerable<string> headerValues;
                var ClientId = 0;
                if (Request.Headers.TryGetValues("ClientId", out headerValues))
                {
                    ClientId = Convert.ToInt32(headerValues.FirstOrDefault());
                }

                return ClientId;
            }
            catch (Exception)
            {
                return 0;
            }
        }
    }
}
