﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using SynchonousAPI.Models;
using System.Net.Http;
using System.Net;

namespace SynchonousAPI.Filter
{
    public class ValidateAPIKey: ActionFilterAttribute
    {
        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            try
            {
                if(!actionContext.Request.Headers.Contains("api_key"))
                {
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, new { Desc = "API Key missing in header" }, actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
                    return;
                }
                if (!actionContext.Request.Headers.Contains("clientid"))
                {
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, new { Desc = "clientId missing in header" }, actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
                    return;
                }
                IEnumerable<string> apiKey = actionContext.Request.Headers.GetValues("api_key");
                IEnumerable<string> clientId = actionContext.Request.Headers.GetValues("clientid");
                if (apiKey != null && !string.IsNullOrWhiteSpace(apiKey.FirstOrDefault()) && clientId!=null)
                {
                    if (!Utility.CompairAPIKey(apiKey.FirstOrDefault(),clientId.FirstOrDefault()))
                    {
                        actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, new { Desc = "API Key/ClientId invalid" }, actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
                        return;
                    }
                }
                else
                {
                    actionContext.Response = actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, new { Desc = "API Key/ClientId missing in header" }, actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
                    return;
                }
            }
            catch (Exception ex)
            {
                actionContext.Response= actionContext.Request.CreateResponse(HttpStatusCode.BadRequest, new { Desc = "API Key : "+ex.Message },actionContext.ControllerContext.Configuration.Formatters.JsonFormatter);
            }
        }
    }
}