﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Web.Http;
using Newtonsoft.Json;
using System.Threading.Tasks;

namespace SynchonousAPI
{
    public static class MaisonetteAPI
    {
        private const string baseAddress = "http://localhost:51903";
        private const string apiKey = "5CFF15E3-8AB1-434E-A153-3D0DB6AB24F4";

        private static HttpResponseMessage getAsync(string url, string contentType)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(baseAddress);
                    client.DefaultRequestHeaders.Add("api_key", apiKey);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue(contentType));
                    HttpResponseMessage resp = client.GetAsync(url).Result;
                    //This method throws an exception if the HTTP response status is an error code.  
                    resp.EnsureSuccessStatusCode();
                    return resp;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }
        private static  HttpResponseMessage postAsync(string url, string contentType)
        {
            try
            {
                using (HttpClient client = new HttpClient())
                {
                    client.BaseAddress = new Uri(baseAddress);
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Add("api_key", apiKey);
                    //client.DefaultRequestHeaders.Add("Content-Type", contentType);
                   
                    StringContent cont = new StringContent("919682496133",Encoding.UTF8, "application/json");
                    HttpResponseMessage resp =  client.PostAsync(url,cont).Result;
                    //This method throws an exception if the HTTP response status is an error code.  
                    resp.EnsureSuccessStatusCode();
                    return resp;
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        public static otpResponse SendOTP(string url, string mobile, string contentType = "application/json")
        {
            otpResponse res = new otpResponse();
            HttpResponseMessage response =  postAsync(url, contentType);
            if (response.IsSuccessStatusCode)
            {
               
               string val= response.Content.ReadAsStringAsync().Result;
                res = JsonConvert.DeserializeObject<otpResponse>(val);
                return res;
            }
            else
            {
                return res;
            }
        }

        public class otpResponse
        {
            public int statusCode { get; set; }
            public int statusText { get; set; }
            public string desc { get; set; }
            public data data { get; set; }
        }

        public class data
        {
            public string messageId { get; set; }
            public string sentTime { get; set; }
        }
    }

   
}