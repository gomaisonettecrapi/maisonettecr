﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using DataModel;
using System.Net;
using ComplaintAPI.Models;

namespace ComplaintAPI.Controllers
{
    [Authorize]
    public class AuthController : ApiController
    {
        [HttpPost]
        public HttpResponseMessage Login([FromBody] string User_name, string Password)
        {
            if (!string.IsNullOrEmpty(User_name) && !string.IsNullOrEmpty(Password))
            {
                using (MaisonetteEntities entity = new MaisonetteEntities())
                {
                    var _result = entity.Logins.Where(x => x.Mobile.Equals(User_name) && x.Password.Equals(Password));
                    if (_result != null)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, 0);
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.Unauthorized, GenericResponse.ErrorDesc="Username or Password is invalid");
                }
            }
            else
                return Request.CreateResponse(HttpStatusCode.Unauthorized, GenericResponse.ErrorDesc = "Username or Password are not supplied");

        }
    }
}