﻿namespace ComplainAPI.Controllers
{
    using ComplainAPI.Filter;
    using ComplainAPI.Models;
    using DataModel;
    using System;
    using System.Collections.Generic;
    using System.Collections.Specialized;
    using System.Data.Entity;
    using System.Data.SqlClient;
    using System.Drawing;
    using System.IO;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web;
    using System.Web.Http;
    using System.Web.Http.ModelBinding;
    using Newtonsoft.Json;
    using System.Threading;

    /// <summary>
    /// This controller contain all tickets Related action methods
    /// </summary>
    [ValidateAPIKey]
    [RoutePrefix("api/Ticket")]
    [Authorize]
    public class TicketController : ApiController
    {
        /// <summary>
        /// it will expose all tickets data for a particular tenant
        /// </summary>
        /// <param name="inputParameter">TenantModel</param>
        /// <returns>Collection of tickets in JSON format</returns>
        [HttpPost]
        [Route("TicketsList")]
        public HttpResponseMessage TicketList([FromBody] TicketListModel inputParameter)
        {
            try
            {
                if (inputParameter != null && ModelState.IsValid)
                {
                    MaisonetteEntities entity = new MaisonetteEntities();

                    var result = (
                                    from tics in entity.Tickets
                                    join req in entity.RequestTypeMasters on tics.RequestTypeId equals req.RequestTypeId
                                    join status in entity.TicketStatusMasters on tics.StatusId equals status.Id
                                    where req.IsActive && status.IsActive && tics.TenantId == inputParameter.TenantId && tics.IsActive==true && tics.ClientId.Equals(req.ClientId) && ((inputParameter.isOpenTickets && tics.StatusId < (int)TicketStatus.Closed) || (!inputParameter.isOpenTickets )) && tics.ClientId.Equals(inputParameter.ClientId) && req.ClientId.Equals(inputParameter.ClientId) //&& status.ClientId.Equals(inputParameter.ClientId)
                                    orderby tics.Id descending
                                    select new { tics.TenantId, tics.TicketNo, TicketId = tics.Id, tics.RequestTitle, tics.RequestDecription, tics.Priority, tics.ClientId, status.StatusType, tics.IsSync, req.RequestTypeId, req.RequestType, tics.CreationDate, tics.LocationType, tics.LocationId }

                                 );

                    if (result != null && result.ToList().Count > 0)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataFound, "Record found", result));
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "No ticket found for user Id : " + inputParameter.TenantId, null));
                    }

                }
                else
                    return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Tenant Id", GetErrorList()));
            }
            catch (Exception ex)
            {
                return CatchException(ex);
            }
        }

        [HttpPost]
        [Route("TicketDetails")]
        public HttpResponseMessage TicketDetails([FromBody] TicketDetailsModel input)
        {
            if (input != null && ModelState.IsValid)
            {
                try
                {
                    MaisonetteEntities entity = new MaisonetteEntities();
                    var ticketDetails = (
                                from tics in entity.Tickets
                                from req in entity.RequestTypeMasters.Where(mapping => mapping.RequestTypeId == tics.RequestTypeId)
                                from status in entity.TicketStatusMasters.Where(mapping => mapping.Id == tics.StatusId)
                                from unt in entity.Units.Where(mapping => mapping.UnitId == tics.LocationId).DefaultIfEmpty()
                                from compo in entity.Components.Where(mapping => mapping.ComponentId == tics.LocationId).DefaultIfEmpty()
                                from feedback in entity.ServiceFeedbacks.Where(mapping => mapping.TicketID == tics.Id).DefaultIfEmpty()
                                where req.IsActive && status.IsActive && tics.Id == input.TicketId && tics.ClientId == input.ClientId
                                select new
                                {
                                    tics.TenantId,
                                    tics.TicketNo,
                                    TicketId = tics.Id,
                                    tics.RequestTitle,
                                    tics.RequestDecription,
                                    tics.Priority,
                                    tics.ClientId,
                                    status.StatusType,
                                    tics.IsSync,
                                    req.RequestTypeId,
                                    req.RequestType,
                                    tics.CreationDate,
                                    tics.LocationType,
                                    tics.LocationId,
                                    LocationIdName = (tics.LocationType == 1 ? compo.ComponentName : unt.UnitNumber),
                                    isFeedbackSubmitted = feedback == null ? false : true,
                                    feedbackId = feedback == null ? 0 : feedback.FeedbackId,
                                    tics.StatusId
                                }
                             );
                    if (ticketDetails != null && ticketDetails.ToList().Count() > 0)
                    {
                        IEnumerable<Attachment> attachmentrow = entity.Attachments.Where(column => column.TicketId.Equals(input.TicketId) && column.IsActive && column.ClientID.Equals(input.ClientId)).ToList();
                        List<ThumbnailModel> thumbnailList = new List<ThumbnailModel>();
                        if (attachmentrow != null && attachmentrow.Count() > 0)
                        {

                            foreach (Attachment currentAttachment in attachmentrow)
                            {
                                ThumbnailModel listItem = new ThumbnailModel();
                                byte[] imgData = currentAttachment.Attachment1;
                                Image img = Utility.byteArrayToImage(imgData);
                                Image thumbImg = img.GetThumbnailImage(input.Size, input.Size, () => false, IntPtr.Zero);
                                listItem.AttachmentId = currentAttachment.Id;
                                listItem.thumbnail = Utility.imageToByteArray(thumbImg);
                                thumbnailList.Add(listItem);
                            }
                        }
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataFound, "Record found", new { TicketDetails = ticketDetails.ToList()[0], Thumbnails = thumbnailList }));
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "No ticket found for Ticket Id : " + input.TicketId + " and ClientId : " + input.ClientId, null));
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Ticket Id", GetErrorList()));

        }

        [HttpPost]
        [Route("UpdateStatus")]
        public HttpResponseMessage UpdateStatus([FromBody] StatusModel input)
        {
            if (input != null && ModelState.IsValid)
            {
                try
                {
                    using (MaisonetteEntities entity = new MaisonetteEntities())
                    {
                        var ticket = entity.Tickets.Where(x => x.Id == input.TicketId).FirstOrDefault();
                        if (ticket != null)
                        {
                            var isValidStatusId = entity.TicketStatusMasters.Where(x => x.Id == input.StatusId && x.IsActive == true).FirstOrDefault();
                            if (isValidStatusId != null)
                            {
                                ticket.StatusId = input.StatusId;
                                ticket.ModificationDate = DateTime.Now;
                                ticket.IsSync = false;
                                entity.Entry(ticket).State = EntityState.Modified;
                                int SaveRows = entity.SaveChanges();
                                if (SaveRows > 0)
                                {
                                    #region Send Push Notification
                                    var login = entity.Logins.Where(x => x.TenantId.Equals(ticket.TenantId) && x.ClientId.Equals(ticket.ClientId)).FirstOrDefault();
                                    if (login != null)
                                    {
                                        if (!string.IsNullOrEmpty(login.DeviceRegToken))
                                        {
                                            NotificationData data = new NotificationData();
                                            data.NotificationType = NotificationType.TicketStatus.ToString();
                                            data.TicketId = ticket.Id;
                                            data.TenancyStartDate = DateTime.MinValue;
                                            data.TenancyEndDate = DateTime.MinValue;
                                            data.ClientId = login.ClientId;
                                            AndroidNotify androidModel = new AndroidNotify();
                                            androidModel.registration_ids = login.DeviceRegToken;
                                            androidModel.data = data;
                                            androidModel.priority = "High";
                                           

                                            iOSNotify iOsModel = new iOSNotify();
                                            iOsModel.notification = "Ios";
                                            iOsModel.priority = "High";
                                            iOsModel.registration_ids= login.DeviceRegToken;
                                            iOsModel.data = data;

                                            if (login.DeviceType.ToLower()=="android")
                                            Utility.sendNotification(login.DeviceRegToken, androidModel,"Ticket Update","Ticket NO "+ticket.TicketNo+" status has been changed"); 
                                          else
                                                Utility.sendNotification(login.DeviceRegToken, iOsModel, "Ticket Update", "Ticket NO " + ticket.TicketNo + " status has been changed");
                                        }
                                        else
                                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Status updated","Push Notification not sent because Registration Token is exist for this tenant"));
                                    } 
                                    #endregion
                                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Status updated"));
                                }
                                else
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Status not updated"));
                                }
                            }
                            else
                                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "StatusId doesn't exist in master database"));
                        }
                        else
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "No ticket found for Ticket Id : " + input.TicketId, null));
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid TicketId/StatusId Supplied", GetErrorList()));
        }

        [HttpPost]
        [Route("CancelTicket")]
        public HttpResponseMessage CancelTicket([FromBody] TicketCancelModel input)
        {
            if (input != null && ModelState.IsValid)
            {
                try
                {
                    using (MaisonetteEntities entity = new MaisonetteEntities())
                    {
                        var ticket = entity.Tickets.Where(x => x.Id == input.TicketId).FirstOrDefault();
                        if (ticket != null)
                        {
                            var CancelStatusId = entity.TicketStatusMasters.Where(x => x.StatusType.ToLower().Contains("cancel") && x.IsActive == true).FirstOrDefault();
                            if (CancelStatusId != null)
                            {
                                ticket.CancelNote = input.CancelNote;
                                ticket.ModificationDate = DateTime.Now;
                                ticket.StatusId = CancelStatusId.Id;
                                ticket.CancellationDate = DateTime.Now;
                                ticket.IsCancelSync = false;
                                //ticket.IsSync = false;
                                entity.Entry(ticket).State = EntityState.Modified;
                                int SaveRows = entity.SaveChanges();
                                if (SaveRows > 0)
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Ticket has been cancelled", null));
                                }
                                else
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Ticket has not been cancelled", null));
                                }
                            }
                            else
                                return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Cancel status is not mapped in Master data", "make an entry in TicketStatusMaster table for cancel status"));
                        }
                        else
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "No ticket found for Ticket Id : " + input.TicketId, null));
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid TicketId/CancelNote supplied", GetErrorList()));

        }

        [HttpPost]
        [Route("LeaseValidity")]
        public HttpResponseMessage LeaseValidity([FromBody] TenantModel input)
        {
            if (input != null && ModelState.IsValid)
            {
                try
                {
                    using (MaisonetteEntities entity = new MaisonetteEntities())
                    {
                        var unitSet = entity.Units.Where(column =>column.ClientId.Equals(input.ClientId) && column.UnitId.Equals(input.UnitId) && column.TenantId.Equals(input.TenantId) && column.TenancyStart <= DateTime.Now && column.TenancyEnd >= DateTime.Now && column.IsActive).FirstOrDefault();
                        if (unitSet != null)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Tenant has valid Lease Period", new { TenancyStart = unitSet.TenancyStart, TenancyEnd = unitSet.TenancyEnd }));
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Tenant's Lease Period expired/not exist", null));
                        }
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Tenant Id", GetErrorList()));
            }
        }

        /// <summary>
        /// Insert feedback in the database
        /// </summary>
        /// <param name="feedbackInput">FeedbackModel</param>
        /// <returns>No. of inserted record</returns>
        [HttpPost]
        [Route("FeedbackSubmit")]
        public HttpResponseMessage FeedbackSubmit([FromBody] FeedbackModel feedbackInput)
        {
            if (feedbackInput != null && ModelState.IsValid) //// Validation check
            {
                try
                {
                    using (MaisonetteEntities entity = new MaisonetteEntities())
                    {
                        ServiceFeedback newFeedback = new ServiceFeedback(); //// Creating the instance of ServiceFeedback table
                        var clientTable = entity.Clients.Where(column => column.ClientID.Equals(feedbackInput.ClientId)).FirstOrDefault();
                        if (clientTable == null)
                        {
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Client Id is not exist in database", new { ClientId = feedbackInput.ClientId }));
                        }
                        var ticketTable = entity.Tickets.Where(column => column.Id.Equals(feedbackInput.TicketId)).FirstOrDefault();
                        if (ticketTable == null)
                        {
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Ticket Id is not exist in database", new { TicketId = feedbackInput.TicketId }));
                        }
                        /// added by suraj at 24/08/2017
                        //var clientId = entity.Tickets.Where(allRow => allRow.Id.Equals(feedbackInput.TicketId)).Select(x => x.ClientId).FirstOrDefault();

                        newFeedback.ClientID = ticketTable.ClientId;
                        // newFeedback.ClientID = feedbackInput.ClientId;
                        newFeedback.TicketID = feedbackInput.TicketId;
                        newFeedback.Feedback = feedbackInput.Feedback;
                        newFeedback.Rating = feedbackInput.Rating;
                        newFeedback.IsActive = true;
                        newFeedback.IsSync = false;
                        newFeedback.CreationDate = DateTime.Now;
                        newFeedback.ModificationDate = DateTime.Now;
                        entity.ServiceFeedbacks.Add(newFeedback); //// Adding new feedback in table collection
                        int insertCount = entity.SaveChanges(); //// Commit changes into database
                        if (insertCount > 0)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordInserted, "Feedback inserted", new { InsertedRecord = insertCount }));
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Feedback not inserted", new { InsertedRecord = insertCount }));
                        }
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid data supplied", GetErrorList()));
            }
        }

        /// <summary>
        /// To insert Ticket details
        /// </summary>
        /// <param name="model">TicketModel</param>
        /// <returns>no. of inserted record</returns>
        [HttpPost]
        [Route("CreateTicket")]
        public HttpResponseMessage CreateTicket([FromBody] TicketModel ticketsInput)
        {
            //// Validation Check
            if (ticketsInput != null && ModelState.IsValid)
            {
                MaisonetteEntities context = null;
                try
                {
                    context = new MaisonetteEntities();
                    DateTime OneMinTimeSpan = DateTime.Now.AddMinutes(-1);
                    var isSameTicket = context.Tickets.Where(x =>x.IsActive==true && x.ClientId.Equals(ticketsInput.ClientId) && x.TenantId.Equals(ticketsInput.TenantId) && x.RequestTypeId.Equals(ticketsInput.RequestTypeId) &&  x.LocationType.Equals(ticketsInput.LocationType) && x.RequestDecription.Equals(ticketsInput.RequestDecription) && x.CreationDate < DateTime.Now && x.CreationDate > OneMinTimeSpan).FirstOrDefault();

                    if(isSameTicket!=null)
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordInserted, "Same Ticket Created"));
                    }
                    if (ticketsInput.LocationType == 1) //Common Area
                    {
                        var isValidLease = context.Units.Any(x => x.TenancyStart <= DateTime.Now && x.TenancyEnd >= DateTime.Now && x.IsActive && x.ClientId.Equals(ticketsInput.ClientId) && x.TenantId.Equals(ticketsInput.TenantId));
                        if (!isValidLease)
                        {
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.DataExpired, "Unit Lease has been expired."));

                        }
                        var compId = context.Components.Where(mapping => mapping.ComponentId.Equals(ticketsInput.LocationId) && mapping.IsActive).FirstOrDefault(); ;
                        if (compId == null)
                        {
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotMatched, "Location Id: " + ticketsInput.LocationId + " is not mapped in Component Master"));
                        }
                    }
                    else
                    {
                        var unitId = context.Units.Where(mapping => mapping.UnitId.Equals(ticketsInput.LocationId) && mapping.IsActive && mapping.ClientId.Equals(ticketsInput.ClientId) && mapping.TenantId.Equals(ticketsInput.TenantId)).FirstOrDefault();
                        if (unitId == null)
                        {
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotMatched, "Location Id: " + ticketsInput.LocationId + " is not mapped in Unit Master"));
                        }
                        else if (!(unitId.TenancyStart <= DateTime.Now && unitId.TenancyEnd >= DateTime.Now))
                        {
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.DataExpired, "Unit Lease has been expired."));
                        }
                    }


                    //// fetcting  record from db if exists
                    var TenantTableRow = context.Tenants.Where(column => column.TenantId.Equals(ticketsInput.TenantId)).FirstOrDefault();
                    var ClientTableRow = context.Clients.Where(column => column.ClientID.Equals(ticketsInput.ClientId)).FirstOrDefault();
                    var StatusTableRow = context.TicketStatusMasters.Where(column => column.Id.Equals(ticketsInput.StatusId)).FirstOrDefault();
                    var ReqTableRow = context.RequestTypeMasters.Where(column => column.RequestTypeId.Equals(ticketsInput.RequestTypeId)).FirstOrDefault();
                    if (TenantTableRow == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotMatched, "Tenant Id: " + ticketsInput.TenantId + " is not mapped in Tenant Master"));
                    }
                    if (ClientTableRow == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotMatched, "Client Id: " + ticketsInput.ClientId + " is not mapped in Client Master"));
                    }
                    if (StatusTableRow == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotMatched, "Status Id: " + ticketsInput.StatusId + " is not mapped in Ticket Status Masters"));
                    }
                    if (ReqTableRow == null)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotMatched, "Request Type Id: " + ticketsInput.RequestTypeId + " is not mapped in Request Type Master"));
                    }
                   
                        Ticket newTicket = new Ticket();
                        newTicket.ClientId = ticketsInput.ClientId;
                        newTicket.TenantId = ticketsInput.TenantId;
                        newTicket.TicketNo = ticketsInput.TicketNo == null ? "" : ticketsInput.TicketNo;
                        newTicket.TenantId = ticketsInput.TenantId;
                        newTicket.CreationDate = DateTime.Now;
                        newTicket.ModificationDate = DateTime.Now;
                        newTicket.IsSync = false;
                        newTicket.RequestDecription = ticketsInput.RequestDecription.Trim();
                        newTicket.RequestTitle = ticketsInput.RequestTitle.Trim();
                        newTicket.RequestTypeId = ticketsInput.RequestTypeId;
                        newTicket.Priority = ticketsInput.Priority;
                        newTicket.StatusId = ticketsInput.StatusId;
                        newTicket.CancelNote = ticketsInput.CancelNote == null ? "" : ticketsInput.CancelNote;
                        newTicket.LocationType = ticketsInput.LocationType;
                        newTicket.IsActive = ticketsInput.IsAttachment ? false : true; // If Attchement=true then isActive=false else isActive=true
                        newTicket.LocationId = ticketsInput.LocationId;
                        newTicket.IsCancelSync = false;
                        context.Tickets.Add(newTicket);// Unit Info Addedd
                        //// commit data into database
                        int saveRows = context.SaveChanges();
                        if (saveRows > 0)
                        {
                            var ticket = context.Tickets.Where(x => x.Id.Equals(newTicket.Id)).FirstOrDefault();
                            if (ticket == null)
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Ticket not created", new { TicketId = -1 }));
                            }
                                ticket.TicketNo = "TIC" + newTicket.Id;
                                context.Entry(ticket).State = EntityState.Modified;
                                context.SaveChanges();
                                return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordInserted, "Ticket Created successfully", new { TicketId = newTicket.Id }));
                            
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Ticket not created"));
                        }

                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
                finally
                {
                    if (context != null)
                    {
                        context.Dispose();
                    }
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
            }
        }

        /// <summary>  
        /// Upload Document.....  
        /// </summary>        
        /// <returns></returns>  
        [HttpPost]
        [Route("Attachments")]
        public async Task<HttpResponseMessage> MediaUpload(int clientId, int TicketId)
        {
            try
            {
                if (clientId < 1 || TicketId < 1)
                {
                    return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", "ClientId/TicketId should be greater than 0(zero)"));
                }

                // Check if the request contains multipart/form-data.  
                if (!Request.Content.IsMimeMultipartContent())
                {
                    return Request.CreateResponse(HttpStatusCode.UnsupportedMediaType, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Media Type is not Valid", new { MediaType = "Media Type should be Multipart/Form-data" }));
                }

                var provider = await Request.Content.ReadAsMultipartAsync<InMemoryMultipartFormDataStreamProvider>(new InMemoryMultipartFormDataStreamProvider());
                //access form data  
                NameValueCollection formData = provider.FormData;
                //access files  
                IList<HttpContent> files = provider.Files;

                // HttpContent file1 = files[0];
                using (MaisonetteEntities dbContext = new MaisonetteEntities())
                {
                    var fileName = "";
                    long? filesize = 0;
                    string ext = string.Empty;
                    IList<string> AllowedFileExtensions = new List<string> { ".jpg", ".gif", ".png" };
                    int totalFiles=0, totalSaved = 0;
                    foreach (HttpContent currentFile in files)
                    {
                        totalFiles += 1;
                        Stream input = await currentFile.ReadAsStreamAsync();

                        var task = currentFile.ReadAsMultipartAsync(provider).
                             ContinueWith(o =>
                             {
                                 //Select the appropriate content item this assumes only 1 part
                                 var fileContent = currentFile;

                                 if (fileContent != null)
                                 {
                                     fileName = fileContent.Headers.ContentDisposition.FileName.Replace("\"", string.Empty);
                                     filesize = fileContent.Headers.ContentDisposition.Size;
                                     ext = fileName.Substring(fileName.LastIndexOf("."), fileName.Length - (fileName.LastIndexOf("."))).ToLower();

                                 }
                             });
                        long MaxContentLength = 1024 * 1024 * 1; //Size = 1 MB  
                        Thread.Sleep(1000);
                        if (!AllowedFileExtensions.Contains(ext))
                        {
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid file extension", new { AllowedExt = AllowedFileExtensions }));
                        }
                        else if (filesize > MaxContentLength)
                        {
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid file size", new { AllowedSize = "5 MB", UploadedFileSize = (filesize / 1024 / 1024) }));
                        }
                        else
                        {
                            Attachment newTableRow = new Attachment();
                            Image newImg = Image.FromStream(input, true, true);
                            byte[] imgArray = Utility.imageToByteArray(newImg);
                            newTableRow.IsActive = true;
                            newTableRow.Attachment1 = imgArray;
                            newTableRow.ClientID = clientId;
                            newTableRow.CreationDate = DateTime.Now;
                            newTableRow.ModificationDate = DateTime.Now;
                            newTableRow.ModificationDate = DateTime.Now;
                            newTableRow.IsSync = false;
                            newTableRow.TicketId = TicketId;
                            dbContext.Attachments.Add(newTableRow);
                            totalSaved+=dbContext.SaveChanges();
                        }
                    }
                    if (totalFiles == totalSaved)
                    {
                        var ticket = dbContext.Tickets.Where(x => x.Id.Equals(TicketId)).FirstOrDefault();
                        if (ticket == null)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Ticket not found", new { }));
                        }
                        else
                        {
                            ticket.IsActive = true;
                            dbContext.Entry(ticket).State = EntityState.Modified;
                            dbContext.SaveChanges();
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordInserted, "All Attachment has been saved", new { }));
                        }
                    }
                    else
                    {
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Some of the attachments are not saved", new { totalSaved = totalSaved + " out of " + totalFiles }));
                    }
                }
            }
            catch (Exception ex)
            {
                return CatchException(ex);
            }
        }

        /// <summary>  
        /// Upload Document.....  
        /// </summary>        
        /// <returns></returns>  
        [HttpPost]
        [Route("GetThumbnail")]
        public async Task<HttpResponseMessage> GetMedia([FromBody] AttachmentModel inputs)
        {
            if (inputs == null || !ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
            }
            else
            {
                try
                {
                    using (MaisonetteEntities dbContext = new MaisonetteEntities())
                    {
                        IEnumerable<Attachment> attachmentrow = dbContext.Attachments.Where(column => column.ClientID.Equals(inputs.ClientId) && column.TicketId.Equals(inputs.TicketId) && column.IsActive).ToList();
                        if (attachmentrow != null && attachmentrow.Count() > 0)
                        {
                            List<ThumbnailModel> responseList = new List<ThumbnailModel>();
                            foreach (Attachment currentAttachment in attachmentrow)
                            {
                                ThumbnailModel listItem = new ThumbnailModel();
                                byte[] imgData = currentAttachment.Attachment1;
                                Image img = Utility.byteArrayToImage(imgData);
                                Image thumbImg = img.GetThumbnailImage(inputs.Size, inputs.Size, () => false, IntPtr.Zero);
                                listItem.AttachmentId = currentAttachment.Id;
                                listItem.thumbnail = Utility.imageToByteArray(thumbImg);
                                responseList.Add(listItem);
                            }
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataFound, "Ticket is found", responseList));
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "TicketId/ClientId is not found or Ticket is not Active", new { }));
                        }
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
            }
        }

        [HttpPost]
        [Route("DeleteAttachment")]
        public async Task<HttpResponseMessage> DeleteAttachment([FromBody] GetAttachmentModel inputs)
        {
            if (inputs == null || !ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
            }
            else
            {
                try
                {
                    using (MaisonetteEntities dbContext = new MaisonetteEntities())
                    {
                        var attachmentrow = dbContext.Attachments.Where(column => column.Id.Equals(inputs.AttachmentId) && column.IsActive).FirstOrDefault();
                        if (attachmentrow != null)
                        {
                            attachmentrow.IsActive = false;
                            attachmentrow.ModificationDate = DateTime.Now;
                            dbContext.Entry(attachmentrow).State = EntityState.Modified;
                            dbContext.SaveChanges();
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Attachment Deleted", new { }));
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "No attachment found for AttachmentId:" + inputs.AttachmentId, new { }));
                        }
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
            }
        }

        [HttpPost]
        [Route("GetAttachment")]
        public async Task<HttpResponseMessage> GetAttachment([FromBody] GetAttachmentModel inputs)
        {
            if (inputs == null || !ModelState.IsValid)
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Parameter", GetErrorList()));
            }
            else
            {
                try
                {
                    using (MaisonetteEntities dbContext = new MaisonetteEntities())
                    {
                        var attachmentrow = dbContext.Attachments.Where(column => column.Id.Equals(inputs.AttachmentId) && column.IsActive).FirstOrDefault();
                        if (attachmentrow != null)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataFound, "Ticket is found", new { TicketId = attachmentrow.TicketId, AttachmentId = attachmentrow.Id, Attachment = attachmentrow.Attachment1, CreationDate = attachmentrow.CreationDate }));
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "No attachment found for AttachmentId:" + inputs.AttachmentId, new { TicketId = "", AttachmentId = inputs.AttachmentId, Attachment = "", CreationDate = "" }));
                        }
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
            }
        }

        [HttpPost]
        [Route("GetRequestType")]
        public HttpResponseMessage GetRequestType([FromBody] ClientModel input)
        {
            if (input != null && ModelState.IsValid)
            {
                try
                {
                    using (MaisonetteEntities entity = new MaisonetteEntities())
                    {
                        IEnumerable<RequestTypeMaster> clientSet = entity.RequestTypeMasters.Where(column => column.ClientId.Equals(input.ClientId) && column.IsActive).ToList();
                        if (clientSet != null)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataFound, "Request Type Found", clientSet));
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataFound, "Request Type not Found", null));
                        }
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Tenant Id", GetErrorList()));
            }
        }

        private IEnumerable<ModelError> GetErrorList()
        {
            return ModelState.Values.SelectMany(v => v.Errors);
        }

        private HttpResponseMessage CatchException(Exception ex)
        {
            try
            {
                SqlException sqlException = ex.InnerException.InnerException as SqlException;
                if (sqlException != null)
                {
                    switch (sqlException.Number)
                    {
                        case 2601:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.UniqueKeyViolation, "Cant insert Duplicate Key value", sqlException.Message));
                        case 2627:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.PrimaryKeyViolation, "Cant insert Duplicate Key value", sqlException.Message));
                        case 547:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ForeignKeyViolation, "Cant insert which is not exist in Master Data", sqlException.Message));
                        case 515:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.NullValueViolation, "Cant insert Null value", sqlException.Message));
                        default:
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.InvalidValue, "Cant insert Duplicate Key value", sqlException.Message));
                    }
                }
                else
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, GenericStatus.GetGenericStatus(GenericStatusCode.InvalidValue, "Some Invalid Value Supplied" + ex.Message));
            }
            catch (Exception exx)
            {
                return Request.CreateResponse(HttpStatusCode.InternalServerError, GenericStatus.GetGenericStatus(GenericStatusCode.InvalidValue, "Some Invalid Value Supplied" + exx.Message));
            }
        }
    }
}
