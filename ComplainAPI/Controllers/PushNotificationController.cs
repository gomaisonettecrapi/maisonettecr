﻿using ComplainAPI.Filter;
using ComplainAPI.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Web.Http;
using System.Web.Script.Serialization;
using FireSharp;
using System.Web.Http.ModelBinding;
using System.Data.SqlClient;
using DataModel;
using System.Data.Entity;

namespace ComplainAPI.Controllers
{
    [ValidateAPIKey]
    [RoutePrefix("api/PushNotification")]
    [Authorize]
    public class PushNotificationController : ApiController
    {
        [HttpPost]
        [Route("NotifytokenUpdate")]
        [Authorize]
        public HttpResponseMessage NotifytokenUpdate([FromBody] NotifyDetails input)
        {
            if (input != null && ModelState.IsValid)
            {
                try
                {
                    using (MaisonetteEntities entity = new MaisonetteEntities())
                    {
                        var loginCollection = entity.Logins.Where(x => (x.ISDCode + x.Mobile).Equals(input.UserName) && x.TenantId.Equals(input.TenantId) && x.IsActive == true).FirstOrDefault();
                        if (loginCollection != null)
                        {
                            try
                            {
                                loginCollection.DeviceId = input.DeviceId;
                                loginCollection.DeviceRegToken = input.DeviceRegToken;
                                loginCollection.DeviceType = input.DeviceType;
                                loginCollection.ModificationDate = DateTime.Now;
                                entity.Entry(loginCollection).State = EntityState.Modified;
                                int effectedRow = entity.SaveChanges();
                                if (effectedRow > 0)
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.RecordChanged, "Notification Token updated successfully"));
                                }
                                else
                                {
                                    return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Notification Token updatation Failed"));
                                }

                            }
                            catch (Exception ex)
                            {
                                return CatchException(ex);
                            }
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "Tenant Id " + input.TenantId + " is not mapped "));
                        }
                    }
                }
                catch (Exception ex)
                {
                    return CatchException(ex);
                }
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Inpalid Paramater", GetErrorList()));
            }
        }

        private IEnumerable<ModelError> GetErrorList()
        {
            return ModelState.Values.SelectMany(v => v.Errors);
        }

        private HttpResponseMessage CatchException(Exception ex)
        {
            SqlException sqlException = ex.InnerException.InnerException as SqlException;
            if (sqlException != null)
            {
                switch (sqlException.Number)
                {
                    case 2601:
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.UniqueKeyViolation, "Cant insert Duplicate Key value", sqlException.Message));
                    case 2627:
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.PrimaryKeyViolation, "Cant insert Duplicate Key value", sqlException.Message));
                    case 547:
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ForeignKeyViolation, "Cant insert which is not exist in Master Data", sqlException.Message));
                    case 515:
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.NullValueViolation, "Cant insert Null value", sqlException.Message));
                    default:
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.InvalidValue, "Cant insert Duplicate Key value", sqlException.Message));
                }
            }
            else
                return Request.CreateResponse(HttpStatusCode.InternalServerError, GenericStatus.GetGenericStatus(GenericStatusCode.InvalidValue, "Some Invalid Value Supplied" + ex.Message));
        }
    }
}
