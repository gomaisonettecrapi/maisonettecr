﻿namespace ComplainAPI.Controllers
{
    using ComplainAPI.Filter;
    using ComplainAPI.Models;
    using DataModel;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity;
    using System.Data.SqlClient;
    using System.Linq;
    using System.Net;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Web.Http;
    using System.Web.Http.ModelBinding;
    using Twilio;
    using Twilio.Clients;
    using Twilio.Rest.Api.V2010.Account;
    using Twilio.Types;

    /// <summary>
    /// OTP Related controller
    /// </summary>
    [ValidateAPIKey]
    [RoutePrefix("api/otp")]
    public class OTPServiceController : ApiController
    {
        #region Constant Variable
        /// <summary>
        /// SMS Gateway Account ID
        /// </summary>
        private const string AccountSid = "ACfcd25c47b291d9d7746459856b3c23a9";

        /// <summary>
        /// SMS Gateway Authuthentication Token
        /// </summary>
        private const string AuthToken = "2be3c7dd82b85fdd795668280d91488f";

        /// <summary>
        /// SMS Body
        /// </summary>
        private const string MessageBody = "One time Verification code for 'Go Maisonette Mobile App' registration is ";

        /// <summary>
        /// SMS Gateway Sender Mobile No/Shortcode
        /// </summary>
        private const string SenderMobileNo = "+14159171379";

        /// <summary>
        /// SMS Gateway Message unique Id Length
        /// </summary>
        private const int MessageIdLength = 34;

        /// <summary>
        /// Same OTP will be sent within this time frame
        /// </summary>
        private const int SameOTPMinutes = 15;
        #endregion

        /// <summary>
        /// Send OTP to verify the mobile no.
        /// </summary>
        /// <param name="input">UserModel</param>
        /// <returns>Unique Message ID code</returns>
        [HttpPost]
        [Route("sendotp")]
        public HttpResponseMessage sendotp([FromBody] UserModel input)
        {
            if (input != null && ModelState.IsValid)
            {
                try
                {
                    using (MaisonetteEntities dbContext = new MaisonetteEntities())
                    {
                        var isMobileExist = dbContext.Logins.Where(column => (column.ISDCode + column.Mobile).Equals(input.UserName) && column.IsActive).FirstOrDefault();
                        string Sid = string.Empty;
                        if (isMobileExist != null)
                        {
                            if (input.MessageId == null)
                            {
                                Sid=CreateOTP(input.UserName);
                            }
                            else if (input.MessageId != null && input.MessageId.Length >= MessageIdLength)
                            {
                                TwilioClient.Init(AccountSid, AuthToken);
                                var message = MessageResource.Fetch(input.MessageId.Trim());
                                DateTime? otpSentTime = message.DateSent;
                                double timeDifference = DateTime.Now.Subtract(Convert.ToDateTime(otpSentTime.ToString())).TotalMinutes;
                                if (timeDifference <= SameOTPMinutes)
                                {
                                    CreateOTP(input.UserName, message.Body);
                                    Sid = input.MessageId;
                                }
                                else
                                {
                                    Sid = CreateOTP(input.UserName);
                                    //return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataExpired, "MessageId " + input.MessageId + " has been expired please generate new OTP with username/Mobile " + input.UserName, new { MessageId = input.MessageId, SentTime = otpSentTime, IsMobileValid = true }));
                                }
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid Message Id", null));
                            }

                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Successful, "OTP has been sent, use remark value for OTP verification", new { MessageId = Sid, SentTime = DateTime.Now, IsMobileValid = true }));
                        }
                        else
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.Unsuccessful, "Mobile Number is not registered with Maisonette system", new { MessageId = "", SentTime = "", IsMobileValid = false }));
                        }
                    }

                }
                catch (Exception ex)
                {
                    if (ex.HResult == -2146233088)
                    {
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid MessageId", null));
                    }
                    else
                    {
                        return CatchException(ex);
                    }
                }
            }
            else
                return ModelValidator();
        }


        public string CreateOTP(string MobileNo, string inputMessageBody = "")
        {
            MobileNo = MobileNo.Contains("+") ? MobileNo.Trim() : "+" + MobileNo.Trim();
            TwilioClient.Init(AccountSid, AuthToken);

            Random otp = new Random();
            string newOTP = otp.Next(1000, 9999).ToString();
            var message = MessageResource.Create(
                   to: new PhoneNumber(MobileNo),
                   from: new PhoneNumber(SenderMobileNo),
                   body: (inputMessageBody == "" ? MessageBody + newOTP : inputMessageBody));
            return message.Sid;


        }

        /// <summary>
        /// Validate OTP which is send by Send OTP Method
        /// </summary>
        /// <param name="_model">OTPModel</param>
        /// <returns></returns>
        [HttpPost]
        [Route("ValidateOtp")]
        public HttpResponseMessage ValidateOtp([FromBody] OTPModel inputs)
        {
            if (inputs != null && ModelState.IsValid && GenaricValidation.OTP4D(inputs.OTP))
            {
                TwilioClient.Init(AccountSid, AuthToken);
                try
                {
                    var message = MessageResource.Fetch(inputs.MessageSID.Trim());
                    bool isValidOTP = false;
                    DateTime? otpSentTime = message.DateSent;
                    if (otpSentTime != null)
                    {
                        double timeDifference = DateTime.Now.Subtract(Convert.ToDateTime(otpSentTime.ToString())).TotalMinutes;
                        if (timeDifference <= 15)
                        {
                            isValidOTP = message.Body.Contains(inputs.OTP);
                            if (isValidOTP)
                            {
                                using (MaisonetteEntities dbContext = new MaisonetteEntities())
                                {
                                    string username = message.To.Replace("+", "");
                                    var loginTableRow = dbContext.Logins.FirstOrDefault(column => (column.ISDCode + column.Mobile).Equals(username) && column.IsActive);
                                    if (loginTableRow != null)
                                    {
                                        loginTableRow.ModificationDate = DateTime.Now;
                                        loginTableRow.IsVerified = true;
                                        dbContext.Entry(loginTableRow).State = EntityState.Modified;
                                        dbContext.SaveChanges();
                                    }
                                }
                                return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataVerified, "OTP is " + "Verified", new { OtpExpired = !isValidOTP, IsOtpValid = isValidOTP, TimeDifference = timeDifference, SentTime = otpSentTime }));
                            }
                            else
                            {
                                return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotMatched, "OTP is " + "not Verified", new { OtpExpired = false, IsOtpValid = isValidOTP, TimeDifference = timeDifference, SentTime = otpSentTime }));
                            }
                        }
                        else
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataExpired, "OTP has been expired", new { OtpExpired = true, IsOtpValid = false, TimeDifference = timeDifference, SentTime = otpSentTime }));
                    }
                    else
                        return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "OTP is invalid/expired", new { OtpExpired = "", IsOtpValid = "", TimeDifference = "", SentTime = "" }));
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, GenericStatus.GetGenericStatus(GenericStatusCode.ServerCodeCrash, ex.Message, new { OtpExpired = "", IsOtpValid = "", TimeDifference = "", SentTime = "" }));
                }
            }
            else
                return ModelValidator();
        }

        private IEnumerable<ModelError> GetErrorList()
        {
            return ModelState.Values.SelectMany(v => v.Errors);
        }

        private HttpResponseMessage CatchException(Exception ex)
        {
            SqlException sqlException = ex.InnerException.InnerException as SqlException;
            if (sqlException != null)
            {
                switch (sqlException.Number)
                {
                    case 2601:
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.UniqueKeyViolation, "Cant insert Duplicate Key value", sqlException.Message));
                    case 2627:
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.PrimaryKeyViolation, "Cant insert Duplicate Key value", sqlException.Message));
                    case 547:
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ForeignKeyViolation, "Cant insert which is not exist in Master Data", sqlException.Message));
                    case 515:
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.NullValueViolation, "Cant insert Null value", sqlException.Message));
                    default:
                        return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.InvalidValue, "Cant insert Duplicate Key value", sqlException.Message));
                }
            }
            else
                return Request.CreateResponse(HttpStatusCode.InternalServerError, GenericStatus.GetGenericStatus(GenericStatusCode.InvalidValue, "Some Invalid Value Supplied" + ex.Message));
        }

        private HttpResponseMessage ModelValidator()
        {
            return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "Invalid parameter format", GetErrorList()));
        }

    }
}
