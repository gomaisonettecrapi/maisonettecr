﻿using ComplainAPI.Filter;
using ComplainAPI.Models;
using DataModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.ModelBinding;

namespace ComplainAPI.Controllers
{
    [ValidateAPIKey]
    [RoutePrefix("api/users")]
    public class UsersController : ApiController
    {

        /// <summary>
        /// Get user information
        /// </summary>
        /// <param name="input">TenantDetails</param>
        /// <returns>return user info in JSON format</returns>
        [HttpPost]
        [Route("userinfo")]
        [Authorize]
        public HttpResponseMessage userinfo([FromBody] TenantDetails input)
        {
            if (input != null && ModelState.IsValid)
            {
                try
                {
                    using (MaisonetteEntities entity = new MaisonetteEntities())
                    {
                        var tenatRow = entity.Tenants.Where(column => column.TenantId.Equals(input.TenantId) && column.IsActive==true).FirstOrDefault(); // TenantId is treated as UserId

                        if (tenatRow != null)
                        {
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataFound, "User is found in database", tenatRow));
                        }
                        else
                            return Request.CreateResponse(HttpStatusCode.OK, GenericStatus.GetGenericStatus(GenericStatusCode.DataNotFound, "User is not found in database", tenatRow));
                    }
                }
                catch (Exception ex)
                {
                    return Request.CreateResponse(HttpStatusCode.InternalServerError, GenericStatus.GetGenericStatus(GenericStatusCode.ServerCodeCrash, "Server code crashed", ex.Message));
                }
            }
            else
                return Request.CreateResponse(HttpStatusCode.BadRequest, GenericStatus.GetGenericStatus(GenericStatusCode.ValidationError, "invalid User/Tenant id", GetErrorList()));

        }

        private IEnumerable<ModelError> GetErrorList()
        {
            return ModelState.Values.SelectMany(v => v.Errors);
        }
    }
}
