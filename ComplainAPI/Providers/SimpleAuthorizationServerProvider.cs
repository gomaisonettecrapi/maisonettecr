﻿namespace ComplainAPI.Providers
{
    using ComplainAPI.Models;
    using ComplainAPI.Security;
    using DataModel;
    using Microsoft.AspNet.Identity.EntityFramework;
    using Microsoft.Owin.Security.OAuth;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Claims;
    using System.Threading.Tasks;
    using System.Web;
    using System.Data.Entity.Core.Objects;
    using Newtonsoft.Json;
    using Newtonsoft.Json.Linq;
    using System.Web.Http;
    using System.Net;
    using System.IO;
    using System.Web.Script.Serialization;
    using System.Reflection;
    using System.Data.Entity;

    public class SimpleAuthorizationServerProvider : OAuthAuthorizationServerProvider
    {
        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            context.Validated();
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                IEnumerable<string> apiKey = context.Request.Headers.GetValues("api_key");
                IEnumerable<string> username = context.Request.Headers.GetValues("username");
                IEnumerable<string> password = context.Request.Headers.GetValues("password");
                IEnumerable<string> deviceId = context.Request.Headers.GetValues("deviceId");
                IEnumerable<string> deviceType = context.Request.Headers.GetValues("deviceType");
                IEnumerable<string> deviceReqToken = context.Request.Headers.GetValues("deviceRegToken");
                if (apiKey != null && username != null && password != null)
                {
                    if (Utility.CompairAPIKey(apiKey.FirstOrDefault()))
                    {
                        context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { "*" });

                        using (MaisonetteEntities dbContext = new MaisonetteEntities())
                        {
                            string pass = Utility.Decode64String(password.FirstOrDefault());
                            string passHash = Utility.GetHashString(pass);
                            var user = dbContext.Logins.Where(column => (column.ISDCode + column.Mobile).Equals(username.FirstOrDefault()) && column.PasswordHash.Equals(passHash)).FirstOrDefault();

                            if (user == null)
                            {
                                context.SetError("invalid_grant", "The user name or password is incorrect.");
                                return;
                            }
                            else if (!user.IsActive)
                            {
                                context.SetError("inactive_user", "The user is mark as inactive.");
                                return;
                            }
                            else if (!user.IsVerified)
                            {
                                context.SetError("unverified_OTP", "mobile OTP is not verified");
                                return;
                            }
                            else
                            {
                                if (deviceId == null || string.IsNullOrEmpty(deviceId.FirstOrDefault()))
                                {
                                    context.SetError("invalid_deviceId", "deviceId is required in Header");
                                    return;
                                }
                                else if (deviceType == null || string.IsNullOrEmpty(deviceType.FirstOrDefault()))
                                {
                                    context.SetError("invalid_deviceType", "deviceType is required in Header");
                                    return;
                                }
                                else if (deviceReqToken == null || string.IsNullOrEmpty(deviceReqToken.FirstOrDefault()))
                                {
                                    context.SetError("invalid_deviceRegToken", "deviceRegToken is required in Header");
                                    return;
                                }
                                else if (deviceId.FirstOrDefault().Length > 180)
                                {
                                    context.SetError("invalid_deviceId", " deviceId is exceeded");
                                    return;
                                }
                                else if ((deviceType.FirstOrDefault().ToLower().Trim() != "ios") && (deviceType.FirstOrDefault().ToLower().Trim() != "android"))
                                {
                                    context.SetError("invalid_deviceType", "deviceType should be Android/IOS");
                                    return;
                                }
                                else if (deviceReqToken.FirstOrDefault().Length > 500)
                                {
                                    context.SetError("invalid_deviceId", " deviceReqToken is exceeded");
                                    return;
                                }
                                else
                                {
                                    user.DeviceId = deviceId == null ? "" : deviceId.FirstOrDefault();
                                    user.DeviceType = deviceType == null ? "" : deviceType.FirstOrDefault();
                                    user.DeviceRegToken = deviceReqToken == null ? "" : deviceReqToken.FirstOrDefault();
                                    user.ModificationDate = DateTime.Now;
                                    dbContext.Entry(user).State = EntityState.Modified;
                                    dbContext.SaveChanges();
                                }
                            } 
                        }
                    }
                    else
                    {
                        context.SetError("invalid_api_key", "The API Key is incorrect.");
                        return;
                    }

                    var identity = new ClaimsIdentity(context.Options.AuthenticationType);
                    identity.AddClaim(new Claim("sub", username.FirstOrDefault()));
                    identity.AddClaim(new Claim("role", "user"));

                    context.Validated(identity);
                }
                else
                {
                    context.SetError("invalid_api_key-Username-Password", "Please provide API key/username/Password in header with parameter name 'api_key/username/password'");
                    return;
                }
            }
            catch (Exception ex)
            {
                context.SetError("invalid Password encoding scheme", ex.Message);
                return;
            }
        }

        public override Task TokenEndpoint(OAuthTokenEndpointContext context)
        {
            using (MaisonetteEntities entity = new MaisonetteEntities())
            {
                int clientId, tenantId;
                IEnumerable<string> username = context.Request.Headers.GetValues("username");
                var result = (
                               from log in entity.Logins
                               from ten in entity.Tenants.Where(mapping => log.TenantId == mapping.TenantId && mapping.ClientId.Equals(log.ClientId) && mapping.IsActive)
                               from cli in entity.Clients.Where(mapping => log.ClientId == mapping.ClientID && mapping.IsActive)
                               select new
                               {
                                   log.ClientId,
                                   log.TenantId,
                                   log.LastLogin,
                                   log.IsActive,
                                   log.ISDCode,
                                   log.IsVerified,
                                   log.Mobile,
                                   ten.TenantName,
                                   ten.TenantNumber,
                                   ten.Nationality,
                                   ClientContactNumber = cli.ContactNumber,
                                  ClientEmail=cli.Email
                               }
                              ).Where(logCol => (logCol.ISDCode + logCol.Mobile).Equals(username.FirstOrDefault()) && logCol.IsActive);

                var loginData = entity.Logins.Where(mapping => (mapping.ISDCode + mapping.Mobile).Equals(username.FirstOrDefault()) && mapping.IsActive).FirstOrDefault();

                clientId = loginData.ClientId;
                tenantId = loginData.TenantId;

                var unit = (
                                  from unt in entity.Units
                                  from compo in entity.Components.Where(mapping =>mapping.IsActive && unt.ComponentId == mapping.ComponentId && mapping.ClientID.Equals(unt.ClientId))
                                  where compo.IsActive && unt.IsActive && unt.TenantId == tenantId && unt.ClientId==clientId
                                  select new
                                  {
                                      compo.ComponentNumber,
                                      unt.TenancyEnd,
                                      unt.TenancyStart,
                                      unt.UnitId,
                                      unt.UnitNumber,
                                      unt.ClientId,
                                      compo.Nationality,
                                      compo.Address,
                                      compo.City,
                                      compo.PlotNo,
                                      compo.PoBox,
                                      compo.Region,
                                      compo.Sector,
                                      compo.ComponentName,
                                      compo.ComponentId
                                  }
                                  );
                var requestResult = entity.RequestTypeMasters.Where(x => x.ClientId.Equals(clientId) && x.IsActive).ToList();

                context.AdditionalResponseParameters.Add("UserInfo", JsonConvert.SerializeObject(result.FirstOrDefault()));
                context.AdditionalResponseParameters.Add("Units", JsonConvert.SerializeObject(unit.ToList()));
                context.AdditionalResponseParameters.Add("ServiceRequestType", JsonConvert.SerializeObject(requestResult.ToList()));
            }

            return Task.FromResult<object>(null);
        }
    }
}