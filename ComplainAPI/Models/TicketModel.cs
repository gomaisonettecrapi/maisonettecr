﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ComplainAPI.Models
{
    public class TicketListModel
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "TenantId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "TenantId should be greater than 0 (zero)")]
        public int TenantId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "ClientId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "ClientId should be greater than 0 (zero)")]
        public int ClientId { get; set; }
       
        public bool isOpenTickets { get; set; }
    }
    public class TenantModel
    {
        /// <summary>
        /// Get: Retrive TenantId
        /// Set: Set TenantId
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "TenantId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "TenantId should be greater than 0 (zero)")]
        public int TenantId { get; set; }

        //[Required(AllowEmptyStrings = false, ErrorMessage = "ClientId is required")]
        [Range(0, int.MaxValue, ErrorMessage = "ClientId should be greater than 0 (zero)")]
        public int ClientId { get; set; }

       // [Required(AllowEmptyStrings = false, ErrorMessage = "UnitId is required")]
        [Range(0, int.MaxValue, ErrorMessage = "UnitId should be greater than 0 (zero)")]
        public int UnitId { get; set; }
    }

    public class ClientModel
    {
        /// <summary>
        /// Get: Retrive ClientId
        /// Set: Set ClientId
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "ClientId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "ClientId should be greater than 0 (zero)")]
        public int ClientId { get; set; }
    }

    public class StatusModel:TicketIdModel
    {
        /// <summary>
        /// Get: Retrive StatusId
        /// Set: Set StatusId
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "StatusId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "StatusId should be greater than 0 (zero)")]
        public int StatusId { get; set; }
    }

    public class TicketIdModel
    {
        /// <summary>
        /// Get: Retrive TicketId
        /// Set: Set TicketId
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "TicketId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "TicketId should be greater than 0 (zero)")]
        public int TicketId { get; set; }
    }

    public class TicketDetailsModel:TicketIdModel
    {
        /// <summary>
        /// Get: Retrive Size
        /// Set: Set Size
        /// </summary>
        [Range(10, int.MaxValue, ErrorMessage = "Size should be greater than 9")]
        public int Size { get; set; } = 50;

        [Required(AllowEmptyStrings = false, ErrorMessage = "ClientId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "ClientId should be greater than 0 (zero)")]
        public int ClientId { get; set; }
    }

    public class FeedbackModel :TicketIdModel
    {
        /// <summary>
        /// Get: Retrive ClientId
        /// Set: Set ClientId
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "ClientId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "ClientId should be greater than 0 (zero)")]
        public int ClientId { get; set; }
       

        /// <summary>
        /// Get: Retrive Rating
        /// Set: Set Rating
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Rating is required")]
        [Range(1, 5, ErrorMessage = "Rating should be in between 1-5")]
        public int Rating { get; set; }

        /// <summary>
        /// Get: Retrive Feedback
        /// Set: Set Feedback
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "Feedback is required")]
        [StringLength(maximumLength: 200, ErrorMessage = "Max length of feedback is 200 characters")]
        public String Feedback { get; set; }
    }

    /// <summary>
    /// Ticket Table scheme
    /// </summary>
    public class TicketModel: TenantModel
    {
       
        /// <summary>
        /// Get: Retrive ClientId
        /// Set: Set ClientId
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "ClientId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "ClientId should be greater than 0 (zero)")]
        public int ClientId { get; set; }

        /// <summary>
        /// Get: Retrive TicketNo
        /// Set: Set TicketNo
        /// </summary>
        //Required(AllowEmptyStrings = false, ErrorMessage = "TicketNo is required")]
        [StringLength(maximumLength: 20, ErrorMessage = "Max length of TicketNo is 20 characters")]
        public String TicketNo { get; set; } 

        /// <summary>
        /// Get: Retrive RequestTypeId
        /// Set: Set RequestTypeId
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "RequestTypeId is required")]
        [Range(1, int.MaxValue, ErrorMessage = "RequestTypeId should be greater than 0 (zero)")]
        public int RequestTypeId { get; set; }

        /// <summary>
        /// Get: Retrive RequestTitle
        /// Set: Set RequestTitle
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "RequestTitle is required")]
        [StringLength(maximumLength: 100, MinimumLength = 5, ErrorMessage = "Invalid Request Title (5-100 characters)")]
        public String RequestTitle { get; set; }

        /// <summary>
        /// Get: Retrive RequestDecription
        /// Set: Set RequestDecription
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "RequestDecription is required")]
        [StringLength(maximumLength: 500, MinimumLength = 5, ErrorMessage = "Invalid RequestDecription (5-500 characters)")]
        public String RequestDecription { get; set; }

        //[Required(AllowEmptyStrings = false, ErrorMessage = "Priority is required")]
        //[StringLength(maximumLength: 10, MinimumLength = 1, ErrorMessage = "Max length of Priority is 10 characters")]
        public String Priority { get; set; } = "Normal"; // set default value 1 as Normal Priority

        /// <summary>
        /// Get: Retrive StatusId
        /// Set: Set StatusId
        /// </summary>
       // [Required(AllowEmptyStrings = false, ErrorMessage = "StatusId is required")]
        //[Range(1, int.MaxValue, ErrorMessage = "StatusId should be greater than 0 (zero)")]
        public int StatusId { get; set; } = 1; // set default value 1 as Open ticket

        /// <summary>
        /// Get: Retrive CancelNote
        /// Set: Set CancelNote
        /// </summary>
        [StringLength(maximumLength: 200,MinimumLength =0, ErrorMessage = "Max length of Priority is 200 characters")]
        public String CancelNote { get; set; }

        /// <summary>
        /// Get: Retrive LocationType
        /// Set: Set LocationType
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "LocationType is required")]
        [Range(1,2, ErrorMessage = "LocationType should be 1 or 2")]
        public int LocationType { get; set; }

        /// <summary>
        /// Get: Retrive LocationType
        /// Set: Set LocationType
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "LocationId is required")]
        [Range(1,int.MaxValue, ErrorMessage = "LocationId should be greater than 0 (zero)")]
        public int LocationId { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "IsAttachment is required")]
        public bool IsAttachment { get; set; }
    }

    public class TicketCancelModel:TicketIdModel
    {
        /// <summary>
        /// Get: Retrive CancelNote
        /// Set: Set CancelNote
        /// </summary>
        [StringLength(maximumLength: 200, ErrorMessage = "Max length of CancelNote is 200 characters")]
        public String CancelNote { get; set; }
    }

    public class AttachmentModel
    {
        /// <summary>
        /// Get: Retrive ClientId
        /// Set: Set ClientId
        /// </summary>
        [Required(AllowEmptyStrings =false,ErrorMessage ="ClientId Required")]
        [Range(1,int.MaxValue, ErrorMessage = "ClientId should be greater than 0(zero)")]
        public int ClientId { get; set; }

        /// <summary>
        /// Get: Retrive ClientId
        /// Set: Set ClientId
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "TicketId Required")]
        [Range(1, int.MaxValue, ErrorMessage = "TicketId should be greater than 0(zero)")]
        public int TicketId { get; set; }

        /// <summary>
        /// Get: Retrive ClientId
        /// Set: Set ClientId
        /// </summary>
        [Required(AllowEmptyStrings = false, ErrorMessage = "TicketId Required")]
        [Range(10, int.MaxValue, ErrorMessage = "Size should be greater than 10")]
        public int Size { get; set; }
    }

    public class GetAttachmentModel
    {
        [Required(AllowEmptyStrings =false,ErrorMessage ="Attachment id is required")]
        [Range(1,int.MaxValue, ErrorMessage = "Attachment id should be greater than 0(zero)")]
        public int AttachmentId { get; set; }
    }

    public class ThumbnailModel:GetAttachmentModel
    {
        public byte[] thumbnail { get; set; }
    }

    public enum TicketStatus
    {
        AllTickets=0,
        Sent=1,
        Processing,
        InProgress,
        Closed,
        Cancelled
    }
}