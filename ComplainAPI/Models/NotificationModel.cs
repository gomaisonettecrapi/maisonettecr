﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ComplainAPI.Models
{
    public class NotificationModel
    {
        public NotificationData Body { get; set; }
        public string Title { get; set; }
        public bool EnableSound { get; set; }
    }

    public class Notifications
    {
        [Required(AllowEmptyStrings = false, ErrorMessage = "RegToken is required")]
        [StringLength(500, ErrorMessage = "Invalid RegToken (Min: 64 char,Max:500 char", MinimumLength = 1)]
        public string RegToken { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "NotifyTitle is required")]
        [StringLength(100, ErrorMessage = "Invalid NotifyTitle (Min: 5 char,Max:100 char", MinimumLength = 5)]
        public string NotifyTitle { get; set; }

        [StringLength(100, ErrorMessage = "Invalid NotifyMsg (Min: 5 char,Max:100 char", MinimumLength = 5)]
        [Required(AllowEmptyStrings = false, ErrorMessage = "NotifyMsg is required")]
        public string NotifyMsg { get; set; }

        [Required(AllowEmptyStrings = false, ErrorMessage = "NotifySoundEnable is required")]
        public bool NotifySoundEnable { get; set; }
    }

    public class NotificationData
    {
        public string NotificationType { get; set; }
        public int TicketId { get; set; }
        public DateTime TenancyStartDate { get; set; }
        public DateTime TenancyEndDate { get; set; }
        public int ClientId { get; set; }
    }

    public enum NotificationType
    {
        TicketStatus=1,
        LeaseUpdate=2
    }   

    public class AndroidNotify
    {
        public string registration_ids { get; set; }
        public NotificationData data { get; set; }
        public string priority { get; set; }
        public string Title { get; set; }
    }
    public class iOSNotify:AndroidNotify
    {
        public string notification { get; set; }
    }
}