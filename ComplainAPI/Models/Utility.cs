﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Configuration;
using System.Web.Http;
using System.Web.Script.Serialization;

namespace ComplainAPI.Models
{
    public static class Utility
    {
        public static string Decode64String(string encodeValue)
        {
            try
            {
                byte[] data = Convert.FromBase64String(encodeValue);
                return Encoding.UTF8.GetString(data);
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private static byte[] GetHash(string inputString)
        {
            HashAlgorithm algorithm = SHA256.Create();  //or use SHA256.Create();
            return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
            {
                sb.Append(b.ToString("X2"));
            }

            return sb.ToString();
        }

        public static bool CompairAPIKey(string inputString)
        {
            try
            {
                string val = ConfigurationManager.AppSettings["APIKey"].ToString().ToLower();
                return inputString.ToLower().Equals(val);
            }
            catch (Exception ex)
            {

                throw ex;
            }
        }

        public static byte[] imageToByteArray(Image imageIn)
        {
            MemoryStream ms = new MemoryStream();
            imageIn.Save(ms, ImageFormat.Jpeg);
            return ms.ToArray();
        }

        public static Image byteArrayToImage(byte[] byteArrayIn)
        {
            MemoryStream ms = new MemoryStream(byteArrayIn);
            Image returnImage = Image.FromStream(ms);
            return returnImage;
        }

        public static bool sendNotification<T>(string RegToken, T model,string title, string body)
        {
            try
            {
                string applicationID = "AAAAxYa-3FU:APA91bHdCJbIDF0TOBWPPYOQCSAOFNJrjgypIhRzTKnsTe9U4E1La4I4Pq6kTqthhYFZLx3IY-U-Tqt04gWlJq68uVwP-XqZwny_44gR1x1dgjrfPjmEbGsHeT8IcLhVu9KDnQXVzAQ6";

                string senderId = "848369212501";
                WebRequest tRequest = WebRequest.Create("https://fcm.googleapis.com/fcm/send");
                tRequest.Method = "post";
                tRequest.ContentType = "application/json";
                var data = new {
                    notification = new
                    {
                        title = title,
                        body = body,
                    },
                    data=model,
                    to = RegToken
                };
                var serializer = new JavaScriptSerializer();
                var json = serializer.Serialize(data);
                Byte[] byteArray = Encoding.UTF8.GetBytes(json);
                tRequest.Headers.Add(string.Format("Authorization: key={0}", applicationID));
                tRequest.Headers.Add(string.Format("Sender: id={0}", senderId));
                tRequest.ContentLength = byteArray.Length;
                using (Stream dataStream = tRequest.GetRequestStream())
                {
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    using (WebResponse tResponse = tRequest.GetResponse())
                    {
                        using (Stream dataStreamResponse = tResponse.GetResponseStream())
                        {
                            using (StreamReader tReader = new StreamReader(dataStreamResponse))
                            {
                                String sResponseFromServer = tReader.ReadToEnd();
                                string str = sResponseFromServer;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                string str = ex.Message;
            }   
            return true;
        }
    }
}