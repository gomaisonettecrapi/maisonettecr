﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace ComplainAPI.Models
{
    public class TenantDetails
    {
        /// <summary>
        /// Get: retrive TenantId value
        /// Set: set TenantId value
        /// </summary>
        [Required(AllowEmptyStrings =false,ErrorMessage ="TenantId is Required")]
        [Range(1,int.MaxValue,ErrorMessage ="TenantId should be greater than 0(zero)")]
        public int TenantId { get; set; }
    }
}