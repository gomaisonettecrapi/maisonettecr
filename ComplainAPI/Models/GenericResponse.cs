﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace ComplainAPI.Models
{
    public static class GenericResponse
    {
        public static string ErrorDesc { get; set; }
        public static string Result { get; set; }
    }

    public static class GenericErrorResponse
    {
        public static string ErrorDesc { get; set; }
        public static string Result { get; set; }
    }


    public static class GenericStatus
    {
        public static object GetGenericStatus(GenericStatusCode statusCode, string desc)
        {
            return new { statusCode = (int)statusCode, statusText = statusCode.ToString(), Desc = desc };
        }

        public static object GetGenericStatus(GenericStatusCode statusCode, string desc, object data)
        {
            return new { statusCode = (int)statusCode, statusText = statusCode.ToString(), Desc = desc, Data = data };
        }

        public static object GetGenericStatus(GenericStatusCode statusCode, string desc, string remark)
        {
            return new { statusCode = (int)statusCode, statusText = statusCode.ToString(), Desc = desc, Remark = remark };
        }
    }

    public enum GenericStatusCode
    {
        Successful = 700,
        DataFound = 701,
        RecordChanged = 702,
        RecordInserted = 703,
        Unsuccessful = 800,
        InvalidCredential = 801,
        DataNotMatched = 802,
        DataExpired = 803,
        DataVerified = 805,
        DataAlreadyExist = 804,
        DataNotFound = 900,
        Execption = 1000,
        Error = 1100,
        ValidationError = 1101,
        ServerError = 1102,
        ServerCodeCrash = 1103,
        ForeignKeyViolation = 1105,
        PrimaryKeyViolation = 1106,
        NullValueViolation = 1107,
        InvalidValue = 1108,
        UniqueKeyViolation=1109,
        ExternalServiceCrashed = 1104
    };
}